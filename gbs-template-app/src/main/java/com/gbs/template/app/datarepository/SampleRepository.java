package com.gbs.template.app.datarepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.gbs.commons.entity.User;

public interface SampleRepository extends JpaRepository<User, Long> , PagingAndSortingRepository<User, Long>{
	

}
