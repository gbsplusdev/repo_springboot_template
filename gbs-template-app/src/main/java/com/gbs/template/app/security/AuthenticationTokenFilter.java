package com.gbs.template.app.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;

import com.gbs.commons.entity.UserAuthenticationToken;
import com.gbs.commons.repository.UserAuthenticationTokenRepository;
import com.gbs.commons.repository.UserRepository;


//import com.gbs.template.app.commons.cassandraservice.CassandraStoreService;

public class AuthenticationTokenFilter extends UsernamePasswordAuthenticationFilter {

	@Value("${javatab.token.header}")
	private String tokenHeader;

	@Autowired
	private TokenUtils tokenUtils;

	@Autowired
	private UserDetailsService userDetailsService;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private EntryPointUnauthorizedHandler entryPointUnauthorizedHandler;
	
	@Autowired
	private UserAuthenticationTokenRepository userAuthenticationTokenRepository;

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
	//	System.out.println("came");
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		String authToken = httpRequest.getHeader(this.tokenHeader);
		String username = this.tokenUtils.getUsernameFromToken(authToken);
		boolean passing = true;
		if(username == null && authToken != null){
		//	User user = userRepository.findByAuthToken(authToken);
			UserAuthenticationToken user = userAuthenticationTokenRepository.findByAuthenticationToken(authToken);
			
			if(user != null){
				username = user.getUserName();
				if(username != null){
					passing = false;	
				}
			}
		}
		if (passing && username != null && SecurityContextHolder.getContext().getAuthentication() == null) {
			UserDetails userDetails = this.userDetailsService.loadUserByUsername(username);

			if (this.tokenUtils.validateToken(authToken, userDetails)) {
				UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
						userDetails, null, userDetails.getAuthorities());
				authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpRequest));
				SecurityContextHolder.getContext().setAuthentication(authentication);
			}
		}else if(!"gbsToken".equalsIgnoreCase(authToken) && SecurityContextHolder.getContext().getAuthentication() == null){
			if(passing && !httpRequest.getServletPath().contains("swagger") && !httpRequest
					.getServletPath().contains("api-docs")){
				
			//	CassandraStoreService.INSTANCE.persistAPI(httpRequest.getServletPath());
				throw new IOException("Access Denied. Invalid Token");
			}
		}
		
		chain.doFilter(request, response);
		
	}

}
