package com.gbs.template.app.controllers;
/************************************************************************************************************

Author                          :SyamKumar S

Date                            :

Notes/Remarks                   :

Organization					: GBS PLUS Pvt Ltd.

								Windsor Building,

                                Ground floor lower level,

                                TC no. 4/1256(38),

                                Kuravankonam, Kowdiar Village,

                                Trivandrum - 695003


Copyright 2018 (C) GBS PLUS Pvt Ltd. (www.gbs-plus.com)

All rights reserved.
***************************************************************************************************************/
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by nasir on 4/2/16.
 */
public class BaseController {
    protected Logger logger = LoggerFactory.getLogger(this.getClass());
}
