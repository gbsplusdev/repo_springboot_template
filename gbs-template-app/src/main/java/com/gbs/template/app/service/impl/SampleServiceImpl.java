package com.gbs.template.app.service.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.gbs.commons.entity.User;
import com.gbs.template.app.datarepository.SampleRepository;
import com.gbs.template.app.service.SampleService;

@Service
public class SampleServiceImpl implements SampleService{
	
	@Autowired
	SampleRepository sampleRepository;


	@Override
	@Transactional
	public Page<User> getAll(Pageable pageable) {
		return sampleRepository.findAll(pageable);
	}


	@Override
	@Transactional
	public User findOne(Long id) {
		return sampleRepository.findOne(id);
	}

}
