package com.gbs.template.app.security;

/************************************************************************************************************

Author                          :SyamKumar S

Date                            :

Notes/Remarks                   :

Organization					: GBS PLUS Pvt Ltd.

								Windsor Building,

                                Ground floor lower level,

                                TC no. 4/1256(38),

                                Kuravankonam, Kowdiar Village,

                                Trivandrum - 695003


Copyright 2018 (C) GBS PLUS Pvt Ltd. (www.gbs-plus.com)

All rights reserved.
***************************************************************************************************************/
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class EntryPointUnauthorizedHandler implements AuthenticationEntryPoint {

	@Override
	public void commence(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
			AuthenticationException e) throws IOException, ServletException {
		httpServletResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Access Denied");
	}

}
