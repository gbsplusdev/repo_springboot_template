package com.gbs.template.app.pojos;

public class OtpValidate {

	private String message;

	private Long id;

	public OtpValidate(String message, Long id) {
		super();
		this.message = message;
		this.id = id;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
