package com.gbs.template.app.utility;

public class Constants {
	
	public static final String SUCCESS = "SUCCESS";
	
	public static final boolean TRUE = true;
	
	public static final String FALSE = "FALSE";
	
	public static final String FAIL = "FAIL";
	
	public static final boolean FAILED = false;
	
	
}
