package com.gbs.template.app.controllers;

import java.util.Date;

import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gbs.commons.entity.User;
import com.gbs.commons.entity.UserMapping;
import com.gbs.commons.repository.UserRepository;
import com.gbs.commons.services.UserService;
import com.gbs.template.app.pojos.LoginPojo;
import com.gbs.template.app.pojos.OtpValidate;
import com.gbs.template.app.utility.ConstantMessages;
import com.gbs.template.app.utility.Constants;
import com.gbs.template.app.utility.Response;

@RestController
public class UserController {
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "/user/register", method = RequestMethod.POST)
	public Response registerUser(@RequestBody User user) throws Exception {
		Response response = new Response();
		try {
			Random random = new Random();
			int start = 1000;
			int end = 8999;

			String email = user.getEmail();
			String mobileNumber = user.getPhone();
			if (email != null && mobileNumber != null) {

				User isExisting = userRepository.findByEmailOrMobile(email, mobileNumber);
				if (isExisting == null) {
					Long otp = (long) (start + random.nextInt(end));
					user.setOtp(otp);

					PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
					String hashedPassword = passwordEncoder.encode(user.getPassword());

					user.setPassword(hashedPassword);
					user.setOtp(user.getOtp());

					user = userRepository.save(user);

					response.setData(user);
					response.setMessage(Constants.SUCCESS);
					response.setStatus(Constants.TRUE);
				} else {
					response.setData(ConstantMessages.EXISTED);
					response.setMessage(Constants.SUCCESS);
					response.setStatus(Constants.TRUE);
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setData(ConstantMessages.NOTVALID);
			response.setMessage(Constants.FAIL);
			response.setStatus(Constants.FAILED);

		}
		return response;

	}
	

	@RequestMapping(value = "/user/login", method = RequestMethod.POST)
	public Response login(@RequestBody LoginPojo loginPojo) throws Exception {

		Response response = new Response();
		try {
			System.out.println("Username is "+loginPojo.getUserName());
			System.out.println("Username HashCode is  "+loginPojo.getUserName().hashCode());
			User persistedUser = userRepository.findByEmailOrMobile(loginPojo.getUserName(), loginPojo.getUserName());
			System.out.println("persistedUser is "+persistedUser);
			if (persistedUser != null) {

				boolean isActive = persistedUser.getisActivated();
				boolean isBlock = persistedUser.getisBlocked();
				List<UserMapping> persistedUserMappings=persistedUser.getUserMappings();
				if (isActive == true && isBlock == false) {
					if (bCryptPasswordEncoder.matches(loginPojo.getPassword(), persistedUser.getPassword())) {
						String userName = null;
						if (persistedUser.getEmail() != null) {
							userName = persistedUser.getEmail();
						} else {
							userName = persistedUser.getPhone();
						}
						Authentication authentication = this.authenticationManager.authenticate(
								new UsernamePasswordAuthenticationToken(userName, loginPojo.getPassword()));
						SecurityContextHolder.getContext().setAuthentication(authentication);
						persistedUser.setDeviceToken(loginPojo.getDeviceToken());
						persistedUser.setDeviceType(loginPojo.getDeviceType());
						persistedUser.setDeviceId(loginPojo.getDeviceId());
						Date lastLoggedInDate = persistedUser.getLastLoggedInDate();
						persistedUser.setLastLoggedInDate(new Date());
						persistedUser = userRepository.save(persistedUser);
						persistedUser.setLastLoggedInDate(lastLoggedInDate);
						if (persistedUser != null) {
							response.setData(persistedUser);
							response.setMessage(Constants.SUCCESS);
							response.setStatus(Constants.TRUE);
						}
					} else {
						OtpValidate otpValidate = new OtpValidate(ConstantMessages.CANNOTLOGIN, persistedUser.getId());
						response.setData(otpValidate);
						response.setMessage(Constants.FAIL);
						response.setStatus(Constants.FAILED);
					}
				} else {
					OtpValidate otpValidate = new OtpValidate(ConstantMessages.ACCOUNTNOTACTIVE, persistedUser.getId());
					response.setData(otpValidate);
					response.setMessage(Constants.FAIL);
					response.setStatus(Constants.FAILED);
				}

			} else {

				OtpValidate otpValidate = new OtpValidate(ConstantMessages.INVALIDCREDENTIAL, null);
				response.setData(otpValidate);
				response.setMessage(Constants.FAIL);
				response.setStatus(Constants.FAILED);
			}

		} catch (Exception e) {
			e.printStackTrace();
			OtpValidate otpValidate = new OtpValidate(ConstantMessages.CANNOTLOGIN, null);
			response.setData(otpValidate);
			response.setMessage(Constants.FAIL);
			response.setStatus(Constants.FAILED);
		}
		return response;
	}	
	
	
	@RequestMapping(value = "/user/logOut", method = RequestMethod.PUT)
	public Response logOut(Long userId) {

		User user = userService.findById(userId);
		user.setDeviceToken(null);
		user = userService.Update(user);
		Response response = new Response();
		response.setData(user);
		response.setMessage(Constants.SUCCESS);
		response.setStatus(Constants.TRUE);
		return response;
	}
}
