package com.gbs.template.app.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gbs.commons.entity.User;
import com.gbs.template.app.service.SampleService;
import com.gbs.template.app.utility.ConstantMessages;
import com.gbs.template.app.utility.Constants;
import com.gbs.template.app.utility.Response;

@RestController
public class SampleController {
	
	@Autowired
	SampleService sampleService;

	@RequestMapping(value = "/gettingUserDetails/list", method = RequestMethod.GET)
	@Transactional(rollbackFor = Exception.class)
	public Response list(Pageable pageable) {
		
		Response response = new Response();
		
		Page<User> user = sampleService.getAll(pageable);
		
		response.setData(user);
		response.setMessage(Constants.SUCCESS);
		response.setStatus(Constants.TRUE);
		
		return response;
	
	}
	
	@RequestMapping(value = "/gettingUserDetails/findById", method = RequestMethod.GET)
	@Transactional(rollbackFor = Exception.class)
	public Response findById(Long id) {
		
		Response response = new Response();
		
		User user = sampleService.findOne(id);
		
		response.setData(user);
		response.setMessage(Constants.SUCCESS);
		response.setStatus(Constants.TRUE);
		
		return response;
	
	}
}
