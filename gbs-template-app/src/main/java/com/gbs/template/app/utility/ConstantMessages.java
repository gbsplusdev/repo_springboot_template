package com.gbs.template.app.utility;

public class ConstantMessages {
	
	public static final String EXISTED = "Data already existed";
	
	public static final String NOTVALID = "Please submit valid data";
	
	public static final String CANNOTLOGIN = "Invalid username and password";
	
	public static final String ACCOUNTNOTACTIVE = "Your Account Is Not Activated";
	
	public static final String INVALIDCREDENTIAL = "Please enter valid credential";

}
