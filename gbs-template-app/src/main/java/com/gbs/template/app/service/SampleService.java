package com.gbs.template.app.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.gbs.commons.entity.User;

public interface SampleService {
	
	public Page<User> getAll(Pageable pageable);
	
	public User findOne(Long id);
}
