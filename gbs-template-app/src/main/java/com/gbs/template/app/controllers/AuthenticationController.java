package com.gbs.template.app.controllers;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/************************************************************************************************************

Author                          :SyamKumar S

Date                            :

Notes/Remarks                   :

Organization					: GBS PLUS Pvt Ltd.

								Windsor Building,

                                Ground floor lower level,

                                TC no. 4/1256(38),

                                Kuravankonam, Kowdiar Village,

                                Trivandrum - 695003


Copyright 2018 (C) GBS PLUS Pvt Ltd. (www.gbs-plus.com)

All rights reserved.
***************************************************************************************************************/
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.mobile.device.Device;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gbs.commons.entity.User;
import com.gbs.commons.entity.UserAuthenticationToken;
import com.gbs.commons.model.SecurityUser;
import com.gbs.commons.repository.UserAuthenticationTokenRepository;
import com.gbs.commons.repository.UserRepository;
import com.gbs.template.app.model.json.request.AuthenticationRequest;
import com.gbs.template.app.model.json.response.AuthenticationResponse;
import com.gbs.template.app.security.TokenUtils;
import com.gbs.template.app.utility.Response;


@RestController
@RequestMapping("${javatab.route.authentication}")
public class AuthenticationController extends BaseController {

	@Value("${javatab.token.header}")
	private String tokenHeader;

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private TokenUtils tokenUtils;

	@Autowired
	private UserDetailsService userDetailsService;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private UserAuthenticationTokenRepository userAuthenticationTokenRepository;

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<?> authenticationRequest(@RequestBody AuthenticationRequest authenticationRequest,
			Device device) throws AuthenticationException {

		// Perform the authentication
		Response response = new Response();
		
		try{
			
		Authentication authentication = this.authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
				authenticationRequest.getUsername(), authenticationRequest.getPassword()));
		SecurityContextHolder.getContext().setAuthentication(authentication);
		
		}catch(Exception e){
			
			response.setStatus(false);
			response.setMessage("Invalid Credential");
			response.setData(null);
			return ResponseEntity.ok(response);
			
		}
		// Reload password post-authentication so we can generate token
		UserDetails userDetails = this.userDetailsService.loadUserByUsername(authenticationRequest.getUsername());
		String token = this.tokenUtils.generateToken(userDetails, device);
		User user = userRepository.findByEmailOrMobile(authenticationRequest.getUsername(), authenticationRequest.getUsername());
		UserAuthenticationToken userAuthenticationToken = new UserAuthenticationToken();
		
		if(user != null ){
			user.setAuthToken(token);
			user.setLastAuthDate(new Date());
			user = userRepository.save(user);
			
			userAuthenticationToken.setUserName(authenticationRequest.getUsername());
			userAuthenticationToken.setAuthenticationToken(token);
			userAuthenticationToken = userAuthenticationTokenRepository.save(userAuthenticationToken);
		}
		// Return the token
		
		response.setStatus(true);
		response.setMessage("SUCCESS");
		response.setData(new AuthenticationResponse(token));
		return ResponseEntity.ok(response);
	}

	@RequestMapping(value = "${javatab.route.authentication.refresh}", method = RequestMethod.GET)
	public ResponseEntity<?> authenticationRequest(HttpServletRequest request) {
		String token = request.getHeader(this.tokenHeader);
		String username = this.tokenUtils.getUsernameFromToken(token);
		SecurityUser user = (SecurityUser) this.userDetailsService.loadUserByUsername(username);
		if (this.tokenUtils.canTokenBeRefreshed(token, user.getLastPasswordReset())) {
			String refreshedToken = this.tokenUtils.refreshToken(token);
			return ResponseEntity.ok(new AuthenticationResponse(refreshedToken));
		} else {
			return ResponseEntity.badRequest().body(null);
		}
	}
}
