package com.gbs.template.app.common.service;

public interface SecurityService {

  public Boolean hasProtectedAccess();

}
