/** ----------------------------------------------------------------------------------------------------------------
 * AUTHOR                          : Sandeep P Pillai
 * DATE                            : 31/01/2018
 * PURPOSE/NOTES                   : Login controller
 * IMPORTANT VARIABLES             :
 * ASSUMPTIONS/LIMITATIONS         :
 * GLOBAL VARIABLES USED           :
 * LIBRARY USED                    :  
 * CLASSES USED                    : 
 * ----------------------------------------------------------------------------------------------------------------
 * REVISION HISTORY
 * ----------------------------------------------------------------------------------------------------------------
 * Date                            By                                      Notes
 * ----------------------------------------------------------------------------------------------------------------
 * 
 * ----------------------------------------------------------------------------------------------------------------
 * PREFIX OF VARIABLES
 * GLOBAL                          : g_
 * CLASS                           : _
 * SUB/FUNCTION/PROPERTY/EVENT     : NIL
 * ARGUMENTS                       : a_ 
 * -----------------------------------------------------------------------------------------------------------------*/

package com.gbs.commons.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.gbs.commons.constants.Mode;
import com.gbs.commons.entity.User;
import com.gbs.commons.exception.DataBaseException;
import com.gbs.commons.exception.ServiceException;
import com.gbs.commons.model.FilterCondition;
import com.gbs.commons.model.Response;
import com.gbs.commons.repository.AbstractRepository;
import com.gbs.commons.services.LoginService;

@Controller
public class LoginController {
	@Autowired
	private LoginService loginService;
	@Autowired
	private AbstractRepository<User, Long> userRepository;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@RequestMapping(value = { "/", "/login" }, method = RequestMethod.GET)
	public ModelAndView login() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("login");
		return modelAndView;
	}

	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String register(Model model) {

		model.addAttribute("user", new User());

		return "register";
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public ModelAndView saveuser(@ModelAttribute User user, BindingResult bindResult, HttpServletRequest request) {

		if (user != null) {

			user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
			userRepository.save(user);
			ModelAndView modelAndView = new ModelAndView();
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			modelAndView.addObject("userName", "Welcome " + auth.getName());
			modelAndView.addObject("adminMessage", "Content Available Only for Users with Admin Role");
			modelAndView.setViewName("admin/home");
			return modelAndView;
		}
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("register");
		return modelAndView;

	}

	@RequestMapping(value = "/admin/home", method = RequestMethod.GET)
	public ModelAndView home(HttpServletRequest request) throws ServiceException, DataBaseException {
		ModelAndView modelAndView = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Response<List<User>> response = new Response<List<User>>();
		FilterCondition filterCondition = new FilterCondition();
		filterCondition.setAttribute("email");
		filterCondition.setOperator("=");
		filterCondition.setValue(auth.getName());
		List<FilterCondition> filterConditions = new ArrayList<FilterCondition>();
		filterConditions.add(filterCondition);
		response = loginService.findAll(filterConditions, userRepository, Mode.INSERT, "", 0L);
		request.getSession().setAttribute("UserID", response.getData());
		modelAndView.addObject("userName", "Welcome " + auth.getName());
		modelAndView.addObject("adminMessage", "Content Available Only for Users with Admin Role");
		modelAndView.setViewName("admin/home");
		return modelAndView;
	}

}
