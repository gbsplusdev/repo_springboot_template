package com.gbs.commons.entity.ui;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.gbs.commons.dao.Identifiable;
import com.gbs.commons.entity.Language;

@Entity
@Table(name = "ui_labels")
public class Label implements Identifiable<Long>, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(nullable = false, length = 25)
	@GeneratedValue(strategy = GenerationType.AUTO)	
	private Long id;
	
	@Version
	private int version;
	
	private String originalLabel;
	
	private String modifiedLabel;
	
	private String originalToolTip;
	
	private String modifiedToolTip;
	
	private String labelId;
	
	@ManyToOne
	@JoinColumn(name="language_id",referencedColumnName="ID")
	@LazyCollection(LazyCollectionOption.FALSE)
	private Language language;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getVersion() {
		return version;
	}
	
	public void setVersion(int version) {
		this.version = version;
	}

	public String getOriginalLabel() {
		return originalLabel;
	}

	public void setOriginalLabel(String originalLabel) {
		this.originalLabel = originalLabel;
	}

	public String getModifiedLabel() {
		return modifiedLabel;
	}

	public void setModifiedLabel(String modifiedLabel) {
		this.modifiedLabel = modifiedLabel;
	}

	public String getLabelId() {
		return labelId;
	}

	public void setLabelId(String labelId) {
		this.labelId = labelId;
	}

	public Language getLanguage() {
		return language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}

	public String getOriginalToolTip() {
		return originalToolTip;
	}

	public void setOriginalToolTip(String originalToolTip) {
		this.originalToolTip = originalToolTip;
	}

	public String getModifiedToolTip() {
		return modifiedToolTip;
	}

	public void setModifiedToolTip(String modifiedToolTip) {
		this.modifiedToolTip = modifiedToolTip;
	}
	
	
}
