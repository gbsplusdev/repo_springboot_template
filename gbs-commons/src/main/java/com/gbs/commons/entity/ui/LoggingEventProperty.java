package com.gbs.commons.entity.ui;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="logging_event_property ")
public class LoggingEventProperty {
	 

	@EmbeddedId
	private LoggingEventPropertyPk logPropertyPk;
	
	@Column(name = "mapped_value", length = 1024)
	private String mapped_value;

	public LoggingEventPropertyPk getLogPropertyPk() {
		return logPropertyPk;
	}

	public void setLogPropertyPk(LoggingEventPropertyPk logPropertyPk) {
		this.logPropertyPk = logPropertyPk;
	}

	public String getMapped_value() {
		return mapped_value;
	}

	public void setMapped_value(String mapped_value) {
		this.mapped_value = mapped_value;
	}
}
