package com.gbs.commons.entity.ui;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "logging_event")
public class LoggingEvent {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "event_id", nullable = false, length = 20)
	private int event_id;

	@Column(name = "timestmp", nullable = false)
	private Double timestmp;

	@Column(name = "formatted_message", nullable = false, length = 4000)
	private String formatted_message;

	@Column(name = "logger_name", nullable = false, length = 254)
	private String logger_name;

	@Column(name = "level_string", nullable = false, length = 254)
	private String level_string;

	@Column(name = "thread_name", length = 254)
	private String thread_name;

	@Column(name = "reference_flag")
	private short reference_flag;

	@Column(name = "arg0", length = 254)
	private String arg0;

	@Column(name = "arg1", length = 254)
	private String arg1;

	@Column(name = "arg2", length = 254)
	private String arg2;

	@Column(name = "arg3", length = 254)
	private String arg3;

	@Column(name = "caller_filename", nullable = false, length = 254)
	private String caller_filename;

	@Column(name = "caller_class", nullable = false, length = 254)
	private String caller_class;

	@Column(name = "caller_method", nullable = false, length = 254)
	private String caller_method;

	@Column(name = "caller_line", nullable = false, length = 4)
	private String caller_line;

	public int getEvent_id() {
		return event_id;
	}

	public void setEvent_id(int event_id) {
		this.event_id = event_id;
	}

	public Double getTimestmp() {
		return timestmp;
	}

	public void setTimestmp(Double timestmp) {
		this.timestmp = timestmp;
	}

	public String getFormatted_message() {
		return formatted_message;
	}

	public void setFormatted_message(String formatted_message) {
		this.formatted_message = formatted_message;
	}

	public String getLogger_name() {
		return logger_name;
	}

	public void setLogger_name(String logger_name) {
		this.logger_name = logger_name;
	}

	public String getLevel_string() {
		return level_string;
	}

	public void setLevel_string(String level_string) {
		this.level_string = level_string;
	}

	public String getThread_name() {
		return thread_name;
	}

	public void setThread_name(String thread_name) {
		this.thread_name = thread_name;
	}

	public short getReference_flag() {
		return reference_flag;
	}

	public void setReference_flag(short reference_flag) {
		this.reference_flag = reference_flag;
	}

	public String getArg0() {
		return arg0;
	}

	public void setArg0(String arg0) {
		this.arg0 = arg0;
	}

	public String getArg1() {
		return arg1;
	}

	public void setArg1(String arg1) {
		this.arg1 = arg1;
	}

	public String getArg2() {
		return arg2;
	}

	public void setArg2(String arg2) {
		this.arg2 = arg2;
	}

	public String getArg3() {
		return arg3;
	}

	public void setArg3(String arg3) {
		this.arg3 = arg3;
	}

	public String getCaller_filename() {
		return caller_filename;
	}

	public void setCaller_filename(String caller_filename) {
		this.caller_filename = caller_filename;
	}

	public String getCaller_class() {
		return caller_class;
	}

	public void setCaller_class(String caller_class) {
		this.caller_class = caller_class;
	}

	public String getCaller_method() {
		return caller_method;
	}

	public void setCaller_method(String caller_method) {
		this.caller_method = caller_method;
	}

	public String getCaller_line() {
		return caller_line;
	}

	public void setCaller_line(String caller_line) {
		this.caller_line = caller_line;
	}

	

}
