package com.gbs.commons.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import com.gbs.commons.constants.Mode;
import com.gbs.commons.dao.Identifiable;
import com.gbs.commons.model.AbstractEntity;
import com.gbs.commons.model.Response;

@Entity
@Table(name="role_password_characteristics")
public class RolePasswordCharacteristics extends AbstractEntity<RolePasswordCharacteristics> implements Identifiable<Long>, Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(nullable=false,length=25)
	private long id; 

	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "roleId")
	private Role role;
	
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "passwordCharacteristicsId")
	private PasswordCharacteristics passwordCharacteristics;

	@Version
	private int version;

	public void setVersion(int version) {
		this.version = version;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public PasswordCharacteristics getPasswordCharacteristics() {
		return passwordCharacteristics;
	}

	public void setPasswordCharacteristics(PasswordCharacteristics passwordCharacteristics) {
		this.passwordCharacteristics = passwordCharacteristics;
	}

	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return id;
	}

	@Override
	public int getVersion() {
		// TODO Auto-generated method stub
		return version;
	}

	@Override
	public String getPrimaryKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Response<?> validate(RolePasswordCharacteristics entity, Mode mode) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}
