package com.gbs.commons.entity.ui;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "MuzSerialNo")
public class MuzSerialNo {

	@Id 
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int alternateKey;

	@OneToOne(targetEntity = MuzSerialNoSettings.class)
	@JoinColumn(name = "settingsID")
	private MuzSerialNoSettings settingID;

	@Column(name = "companyID")
	private int companyID;

	@Column(name = "branchID")
	private int branchID;

	@Column(name = "locationID")
	private int locationID;

	@Column(name = "finYearID")
	private int finYearID;

	@Column(name = "userID")
	private int userID;

	@Column(name = "lastSLNo")
	private int lastSLNo;

	@Column(name = "prefix")
	private String prefix;

	@Column(name = "entityHierarchyID")
	private Integer entityHierarchyID;

	public int getAlternateKey() {
		return alternateKey;
	}

	public void setAlternateKey(int alternateKey) {
		this.alternateKey = alternateKey;
	}

	public MuzSerialNoSettings getSettingID() {
		return settingID;
	}

	public void setSettingID(MuzSerialNoSettings settingID) {
		this.settingID = settingID;
	}

	public int getCompanyID() {
		return companyID;
	}

	public void setCompanyID(int companyID) {
		this.companyID = companyID;
	}

	public int getBranchID() {
		return branchID;
	}

	public void setBranchID(int branchID) {
		this.branchID = branchID;
	}

	public int getLocationID() {
		return locationID;
	}

	public void setLocationID(int locationID) {
		this.locationID = locationID;
	}

	public int getFinYearID() {
		return finYearID;
	}

	public void setFinYearID(int finYearID) {
		this.finYearID = finYearID;
	}

	public int getUserID() {
		return userID;
	}

	public void setUserID(int userID) {
		this.userID = userID;
	}

	public int getLastSLNo() {
		return lastSLNo;
	}

	public void setLastSLNo(int lastSLNo) {
		this.lastSLNo = lastSLNo;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public Integer getEntityHierarchyID() {
		return entityHierarchyID;
	}

	public void setEntityHierarchyID(Integer entityHierarchyID) {
		this.entityHierarchyID = entityHierarchyID;
	}

}
