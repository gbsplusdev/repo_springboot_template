package com.gbs.commons.entity.ui;

import java.util.ArrayList;
import java.util.List;

import com.gbs.commons.entity.KeyValuePairs;
import com.gbs.commons.util.KeyValuePair;

public enum ScreenMode implements KeyValuePairs {
	
	ALL("A"),
	UPDATE("U"),
	VIEW("V");
	
	private String key;
	
	private ScreenMode(String key) {
		this.key= key;
	}

	public String getKey() {
		return key;
	}

	@Override
	public List<KeyValuePair<String, String>> getKeyValuePairs() {
		List<KeyValuePair<String, String>> list = new ArrayList<KeyValuePair<String, String>>();
		for (ScreenMode mode : values()) {
			KeyValuePair<String, String> kvp = new KeyValuePair<String, String>(mode.name(), mode.key);
			list.add(kvp);
		}
		return list;
	}
	
	public static ScreenMode getMode(String key) {
		ScreenMode mode = null;
		for (ScreenMode screenMode : values()) {
			if(key.equalsIgnoreCase(screenMode.getKey())) {
				mode = screenMode;
			}
		}
		return mode;
	}
	
	

}
