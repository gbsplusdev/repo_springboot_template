package com.gbs.commons.entity;

import java.util.ArrayList;
import java.util.List;

import com.gbs.commons.util.KeyValuePair;

public enum MessageType implements KeyValuePairs {
	
	ERROR("Error","E"),
	WARNING("Warning","W"),
	INFO("Info","I");
	
	private String value;
	private String key;

	private MessageType(String value,String key) {
		this.value = value;
		this.key = key;
	}

	public String getValue() {
		return value;
	}
	

	public String getKey() {
		return key;
	}

	public List<KeyValuePair<String, String>> getKeyValuePairs() {
		List<KeyValuePair<String, String>> list = new ArrayList<KeyValuePair<String, String>>();
		for (MessageType messageType : values()) {
			KeyValuePair<String, String> kvp = new KeyValuePair<String, String>(messageType.name(), messageType.value);
			list.add(kvp);
		}
		return list;
	}
	
	public static MessageType getByKey(String key) {
		MessageType type = null;
		for (MessageType messageType : values()) {
			if(messageType.key.equalsIgnoreCase(key)) {
				type = messageType;
			}
		}
		return type;
	}
	
}
