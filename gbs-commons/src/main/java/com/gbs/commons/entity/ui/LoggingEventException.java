package com.gbs.commons.entity.ui;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "logging_event_exception")
public class LoggingEventException {

	@EmbeddedId
	private LoggingEventExceptionPk logExceptionPk;

	@Column(name = "trace_line", nullable = false, length = 254)
	private String trace_line;

	public LoggingEventExceptionPk getLogExceptionPk() {
		return logExceptionPk;
	}

	public void setLogExceptionPk(LoggingEventExceptionPk logExceptionPk) {
		this.logExceptionPk = logExceptionPk;
	}

	public String getTrace_line() {
		return trace_line;
	}

	public void setTrace_line(String trace_line) {
		this.trace_line = trace_line;
	}

}
