package com.gbs.commons.entity;

public class PasswordStatus {

	public boolean isValid() {
		return valid;
	}
	public void setValid(boolean valid) {
		this.valid = valid;
	}
	public boolean isExpired() {
		return expired;
	}
	public void setExpired(boolean expired) {
		this.expired = expired;
	}
	public boolean isLocked() {
		return locked;
	}
	public void setLocked(boolean locked) {
		this.locked = locked;
	}
	public int getExpiryInDays() {
		return expiryInDays;
	}
	public void setExpiryInDays(int expiryInDays) {
		this.expiryInDays = expiryInDays;
	}
	public int getWarningInDays() {
		return warningInDays;
	}
	public void setWarningInDays(int warningInDays) {
		this.warningInDays = warningInDays;
	}
	private boolean valid;
	private boolean expired;
	private boolean locked;
	private int expiryInDays;
	private int warningInDays;
	@Override
	public String toString() {
		return "PasswordStatus [valid=" + valid + ", expired=" + expired + ", locked=" + locked + ", expiryInDays="
				+ expiryInDays + ", warningInDays=" + warningInDays + "]";
	}
	
}
