package com.gbs.commons.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

import com.gbs.commons.constants.Mode;
import com.gbs.commons.dao.Identifiable;
import com.gbs.commons.model.AbstractEntity;
import com.gbs.commons.model.Response;

@Entity
@Table(name="password_characteristics")
public class PasswordCharacteristics extends AbstractEntity<PasswordCharacteristics> implements Identifiable<Long>, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(nullable = false, length = 25)
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private int minLenth;
	
	private int maxLength;
	
	private int noOfUpperCase;
	
	private int noOfLowerCase;
	private int noOfNumeric;
	
	private int noOfNonAlphaNumerics;
	
	private int expiryPeriod;
	private int warningPeriod;
	
	private int loginAttempts;
	
	@Version
	private int version;

	public void setId(long id) {
		this.id = id;
	}

	public int getNoOfNonAlphaNumerics() {
		return noOfNonAlphaNumerics;
	}

	public void setNoOfNonAlphaNumerics(int noOfNonAlphaNumerics) {
		this.noOfNonAlphaNumerics = noOfNonAlphaNumerics;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public int getMinLenth() {
		return minLenth;
	}

	public void setMinLenth(int minLenth) {
		this.minLenth = minLenth;
	}

	public int getMaxLength() {
		return maxLength;
	}

	public void setMaxLength(int maxLength) {
		this.maxLength = maxLength;
	}

	public int getNoOfUpperCase() {
		return noOfUpperCase;
	}

	public void setNoOfUpperCase(int noOfUpperCase) {
		this.noOfUpperCase = noOfUpperCase;
	}

	public int getNoOfLowerCase() {
		return noOfLowerCase;
	}

	public void setNoOfLowerCase(int noOfLowerCase) {
		this.noOfLowerCase = noOfLowerCase;
	}

	public int getNoOfNumeric() {
		return noOfNumeric;
	}

	public void setNoOfNumeric(int noOfNumeric) {
		this.noOfNumeric = noOfNumeric;
	}

	public int getNoOfNonAlphaNumericas() {
		return noOfNonAlphaNumerics;
	}

	public void setNoOfNonAlphaNumericas(int noOfNonAlphaNumericas) {
		this.noOfNonAlphaNumerics = noOfNonAlphaNumericas;
	}

	public int getExpiryPeriod() {
		return expiryPeriod;
	}

	public void setExpiryPeriod(int expiryPeriod) {
		this.expiryPeriod = expiryPeriod;
	}

	public int getWarningPeriod() {
		return warningPeriod;
	}

	public void setWarningPeriod(int warningPeriod) {
		this.warningPeriod = warningPeriod;
	}

	public int getLoginAttempts() {
		return loginAttempts;
	}

	public void setLoginAttempts(int loginAttempts) {
		this.loginAttempts = loginAttempts;
	}

	public int getLockPeriod() {
		return lockPeriod;
	}

	public void setLockPeriod(int lockPeriod) {
		this.lockPeriod = lockPeriod;
	}

	private int lockPeriod;

	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return id;
	}

	@Override
	public int getVersion() {
		// TODO Auto-generated method stub
		return version;
	}

	@Override
	public Response<?> validate(PasswordCharacteristics entity, Mode mode) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getPrimaryKey() {
		// TODO Auto-generated method stub
		return null;
	}
	
	 
}
