package com.gbs.commons.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

import com.gbs.commons.constants.Mode;
import com.gbs.commons.dao.Identifiable;
import com.gbs.commons.model.AbstractEntity;
import com.gbs.commons.model.Response;

@Entity
@Table(name = "role")
public class Role extends AbstractEntity<Role> implements Identifiable<Long>, Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(nullable = false, length = 25)
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Version
	private int version;

	private String name;

	private String description;

	/*
	 * @ManyToMany
	 * 
	 * @LazyCollection(LazyCollectionOption.FALSE)
	 * 
	 * @OrderBy("ordinal asc") private Collection<MenuGroup> menuGroups;
	 * 
	 * @ManyToMany
	 * 
	 * @LazyCollection(LazyCollectionOption.FALSE)
	 * 
	 * @OrderBy("ordinal asc") private Collection<Menu> menus;
	 */

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	/*
	 * public Collection<MenuGroup> getMenuGroups() { return menuGroups; }
	 * 
	 * public void setMenuGroups(Collection<MenuGroup> menuGroups) {
	 * this.menuGroups = menuGroups; }
	 * 
	 * 
	 * public Collection<Menu> getMenus() { return menus; }
	 * 
	 * public void setMenus(Collection<Menu> menus) { this.menus = menus; }
	 */

	@Override
	public String getPrimaryKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Response<?> validate(Role entity, Mode mode) {
		// TODO Auto-generated method stub
		return null;
	}
}
