package com.gbs.commons.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

import com.gbs.commons.constants.Mode;
import com.gbs.commons.dao.Identifiable;
import com.gbs.commons.model.AbstractEntity;
import com.gbs.commons.model.Response;

@Entity
@Table(name="login_failure_attempts")
public class LoginFailureAttempts extends AbstractEntity<LoginFailureAttempts> implements Identifiable<Long>, Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(nullable=false,length=25)
	private long id;
	
	private long userId;
	
	private int failureAttempts;
	
	private Date updatedTime;
	
	private boolean userLocked;
	
	@Version
	private int verison;

	public void setId(long id) {
		this.id = id;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public int getFailureAttempts() {
		return failureAttempts;
	}

	public void setFailureAttempts(int failureAttempts) {
		this.failureAttempts = failureAttempts;
	}

	public Date getUpdatedTime() {
		return updatedTime;
	}

	public void setUpdatedTime(Date updatedTime) {
		this.updatedTime = updatedTime;
	}

	public boolean isUserLocked() {
		return userLocked;
	}

	public void setUserLocked(boolean userLocked) {
		this.userLocked = userLocked;
	}

	@Override
	public Long getId() {
		// TODO Auto-generated method stub
		return id;
	}

	@Override
	public int getVersion() {
		// TODO Auto-generated method stub
		return verison;
	}

	public void setVerison(int verison) {
		this.verison = verison;
	}

	@Override
	public Response<?> validate(LoginFailureAttempts entity, Mode mode) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getPrimaryKey() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	
	
}
