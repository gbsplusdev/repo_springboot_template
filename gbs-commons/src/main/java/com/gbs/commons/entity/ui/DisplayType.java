package com.gbs.commons.entity.ui;

import java.util.ArrayList;
import java.util.List;

import com.gbs.commons.entity.KeyValuePairs;
import com.gbs.commons.util.KeyValuePair;

public enum DisplayType implements KeyValuePairs {
	
	TEXTFIELD("I","Text Field"),
	TEXTAREA("A","Text Area"),
	COMBO_BOX("C","Combo Box"),
	CHECK_BOX("K","Check Box"),
	RADIO_BUTTON("R","Radio Button");
	
	private String value;
	private String key;

	private DisplayType(String key,String value) {
		this.key = key;
		this.value = value;
	}
	
	public static DisplayType getDisplayType(String key) {
		DisplayType type = null;
		for (DisplayType displayType : values()) {
			if(key.equalsIgnoreCase(displayType.getKey())) {
				type = displayType;
			}
		}
		return type;
	}
	
	

	public String getKey() {
		return key;
	}



	public String getValue() {
		return value;
	}

	public List<KeyValuePair<String, String>> getKeyValuePairs() {
		List<KeyValuePair<String, String>> list = new ArrayList<KeyValuePair<String, String>>();
		for (DisplayType displayType : values()) {
			KeyValuePair<String, String> kvp = new KeyValuePair<String, String>(displayType.name(), displayType.value);
			list.add(kvp);
		}
		return list;
	}

}
