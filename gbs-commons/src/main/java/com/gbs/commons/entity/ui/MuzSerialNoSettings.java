package com.gbs.commons.entity.ui;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "MuzSerialNoSettings")
public class MuzSerialNoSettings {
 
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "settingsID")
	private Integer settingsID;

	@Column(name = "optionID")
	private int optionID;

	@Column(name = "transType")
	private String transType;

	@Column(name = "transSubType")
	private String transSubType;

	@Column(name = "companyRequired", length = 1)
	private String companyRequired;

	@Column(name = "branchRequired", length = 1)
	private String branchRequired;

	@Column(name = "locationRequired", length = 1)
	private String locationRequired;

	@Column(name = "finYearRequired", length = 1)
	private String finYearRequired;

	@Column(name = "userRequired", length = 1)
	private String userRequired;

	@Column(name = "prefix", length = 5)
	private String prefix;

	@Column(name = "suffix", length = 5)
	private String suffix;

	@Column(name = "padChar", length = 1)
	private String padChar;

	@Column(name = "sizeOfSerialNo")
	private int sizeOfSerialNo;

	@Column(name = "refSettingsID")
	private Integer refSettingsID;

	@Column(name = "remarks", length = 250)
	private String remarks;

	public Integer getSettingsID() {
		return settingsID;
	}

	public void setSettingsID(Integer settingsID) {
		this.settingsID = settingsID;
	}

	public int getOptionID() {
		return optionID;
	}

	public void setOptionID(int optionID) {
		this.optionID = optionID;
	}

	public String getTransType() {
		return transType;
	}

	public void setTransType(String transType) {
		this.transType = transType;
	}

	public String getTransSubType() {
		return transSubType;
	}

	public void setTransSubType(String transSubType) {
		this.transSubType = transSubType;
	}

	public String getCompanyRequired() {
		return companyRequired;
	}

	public void setCompanyRequired(String companyRequired) {
		this.companyRequired = companyRequired;
	}

	public String getBranchRequired() {
		return branchRequired;
	}

	public void setBranchRequired(String branchRequired) {
		this.branchRequired = branchRequired;
	}

	public String getLocationRequired() {
		return locationRequired;
	}

	public void setLocationRequired(String locationRequired) {
		this.locationRequired = locationRequired;
	}

	public String getFinYearRequired() {
		return finYearRequired;
	}

	public void setFinYearRequired(String finYearRequired) {
		this.finYearRequired = finYearRequired;
	}

	public String getUserRequired() {
		return userRequired;
	}

	public void setUserRequired(String userRequired) {
		this.userRequired = userRequired;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public String getPadChar() {
		return padChar;
	}

	public void setPadChar(String padChar) {
		this.padChar = padChar;
	}

	public int getSizeOfSerialNo() {
		return sizeOfSerialNo;
	}

	public void setSizeOfSerialNo(int sizeOfSerialNo) {
		this.sizeOfSerialNo = sizeOfSerialNo;
	}

	public Integer getRefSettingsID() {
		return refSettingsID;
	}

	public void setRefSettingsID(Integer refSettingsID) {
		this.refSettingsID = refSettingsID;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
}
