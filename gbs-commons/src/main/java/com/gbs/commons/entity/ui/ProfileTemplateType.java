package com.gbs.commons.entity.ui;

import java.util.ArrayList;
import java.util.List;

import com.gbs.commons.entity.KeyValuePairs;
import com.gbs.commons.util.KeyValuePair;

public enum ProfileTemplateType implements KeyValuePairs {
	
	FORM("form"),
	TABULAR("tabular"),
	FORM_TABULAR("form-tabular");
	
	private String value;
	
	private ProfileTemplateType(String value) {
		this.value = value;
	} 
	
	public static ProfileTemplateType getTemplateType(String value) {
		ProfileTemplateType type = null;
		for (ProfileTemplateType profileTemplateType : values()) {
			if(value.equalsIgnoreCase(profileTemplateType.value)) {
				type = profileTemplateType;
			}
		}
		return type;
	}

	@Override
	public List<KeyValuePair<String, String>> getKeyValuePairs() {
		List<KeyValuePair<String, String>> list = new ArrayList<KeyValuePair<String, String>>();
		for (ProfileTemplateType profileTemplateType : values()) {
			KeyValuePair<String, String> kvp = new KeyValuePair<String, String>(profileTemplateType.name(), profileTemplateType.value);
			list.add(kvp);
		}
		return list;
	}

}
