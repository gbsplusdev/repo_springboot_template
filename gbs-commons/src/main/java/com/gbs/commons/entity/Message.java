package com.gbs.commons.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.gbs.commons.dao.Identifiable;

@Entity
@Table(name = "messages")
public class Message implements Identifiable<Long>, Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(nullable = false, length = 25)
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Version
	private int version;
	
	private String originalMessageText;
	
	private String modifiedMessageText;
	
	private String messageId;
	
	private String groupId;
	
	@ManyToOne
	@JoinColumn(name="language_id",referencedColumnName="ID")
	@LazyCollection(LazyCollectionOption.FALSE)
	private Language language;
	
	private MessageType messageType;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getVersion() {
		return version;
	}
	
	public void setVersion(int version) {
		this.version = version;
	}

	public String getOriginalMessageText() {
		return originalMessageText;
	}

	public void setOriginalMessageText(String originalMessageText) {
		this.originalMessageText = originalMessageText;
	}

	public String getModifiedMessageText() {
		return modifiedMessageText;
	}

	public void setModifiedMessageText(String modifiedMessageText) {
		this.modifiedMessageText = modifiedMessageText;
	}

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public Language getLanguage() {
		return language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}

	public MessageType getMessageType() {
		return messageType;
	}

	public void setMessageType(MessageType messageType) {
		this.messageType = messageType;
	}
	
}
