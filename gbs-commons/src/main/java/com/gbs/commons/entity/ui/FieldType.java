package com.gbs.commons.entity.ui;

import java.util.ArrayList;
import java.util.List;

import com.gbs.commons.entity.KeyValuePairs;
import com.gbs.commons.util.KeyValuePair;

public enum FieldType implements KeyValuePairs {
	
	TEXT("T","Text Field"),
	NUMBER("N","Numeric Field"),
	DATE("D","Date Field");
	
	private String value;
	private String key;

	private FieldType(String key ,String value) {
		this.key = key;
		this.value = value;
	}

	public String getValue() {
		return value;
	}
	
	public String getKey() {
		return key;
	}
	
	public static FieldType getFieldType(String key) {
		FieldType type = null;
		for (FieldType fieldType : values()) {
			if(key.equalsIgnoreCase(fieldType.getKey())) {
				type = fieldType;
			}
		}
		return type;
	}

	public List<KeyValuePair<String, String>> getKeyValuePairs() {
		List<KeyValuePair<String, String>> list = new ArrayList<KeyValuePair<String, String>>();
		for (FieldType fieldType : values()) {
			KeyValuePair<String, String> kvp = new KeyValuePair<String, String>(fieldType.name(), fieldType.value);
			list.add(kvp);
		}
		return list;
	}

}
