package com.gbs.commons.entity.ui;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Embeddable
public class LoggingEventPropertyPk implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2145002816770237705L;

	@ManyToOne(targetEntity = LoggingEvent.class, fetch = FetchType.LAZY)
	@JoinColumn(name = "event_id", nullable = false)
	private LoggingEvent event_id;

	@Column(name = "mapped_key", nullable = false, length = 254)
	private String mapped_key;

	public LoggingEvent getEvent_id() {
		return event_id;
	}

	public void setEvent_id(LoggingEvent event_id) {
		this.event_id = event_id;
	}

	public String getMapped_key() {
		return mapped_key;
	}

	public void setMapped_key(String mapped_key) {
		this.mapped_key = mapped_key;
	}

}
