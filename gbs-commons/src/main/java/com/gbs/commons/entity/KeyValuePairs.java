package com.gbs.commons.entity;

import java.util.List;

import com.gbs.commons.util.KeyValuePair;

public interface KeyValuePairs {
	List<KeyValuePair<String, String>> getKeyValuePairs();
}
