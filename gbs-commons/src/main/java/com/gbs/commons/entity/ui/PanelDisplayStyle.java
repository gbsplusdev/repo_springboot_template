package com.gbs.commons.entity.ui;

import java.util.ArrayList;
import java.util.List;

import com.gbs.commons.entity.KeyValuePairs;
import com.gbs.commons.util.KeyValuePair;

public enum PanelDisplayStyle implements KeyValuePairs {
	
	FORM("form"),
	TABULAR("tabular");
	
	private String value;

	private PanelDisplayStyle(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
	
	public static PanelDisplayStyle getDisplayStyle(String value) {
		PanelDisplayStyle type = null;
		for (PanelDisplayStyle panelDisplayStyle : values()) {
			if(value.equalsIgnoreCase(panelDisplayStyle.value)) {
				type = panelDisplayStyle;
			}
		}
		return type;
	}

	public List<KeyValuePair<String, String>> getKeyValuePairs() {
		List<KeyValuePair<String, String>> list = new ArrayList<KeyValuePair<String, String>>();
		for (PanelDisplayStyle displayType : values()) {
			KeyValuePair<String, String> kvp = new KeyValuePair<String, String>(displayType.name(), displayType.value);
			list.add(kvp);
		}
		return list;
	}

}
