package com.gbs.commons.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import com.gbs.commons.constants.Mode;
import com.gbs.commons.dao.Identifiable;
import com.gbs.commons.model.AbstractEntity;
import com.gbs.commons.model.Response;

@Entity
@Table(name="menu_role_mapping")
public class MenuRoleMapping extends AbstractEntity<MenuRoleMapping> implements Identifiable<Long>,Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(nullable = false, length = 25)
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Version
	private int version;
	
	private String description;
	
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "roleId")
	private Role role;
	
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "officeId")
	private Office office;
	
	@OneToOne(fetch=FetchType.EAGER)	
	@JoinColumn(name = "menuConfigId")
	private MenuConfig menuConfig;
	
	public MenuConfig getMenuConfig() {
		return menuConfig;
	}

	public void setMenuConfig(MenuConfig menuConfig) {
		this.menuConfig = menuConfig;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Office getOffice() {
		return office;
	}

	public void setOffice(Office office) {
		this.office = office;
	}
	

	@Override
	public String getPrimaryKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Response<?> validate(MenuRoleMapping entity, Mode mode) {
		// TODO Auto-generated method stub
		return null;
	}
	
	

}
