package com.gbs.commons.entity.ui;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Embeddable
public class LoggingEventExceptionPk implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2942528488806865410L;

	@ManyToOne(targetEntity = LoggingEvent.class, fetch = FetchType.LAZY)
	@JoinColumn(name = "event_id", nullable = false)
	private LoggingEvent event_id;

	@Column(name = "i", nullable = false)
	private short i;

	public LoggingEvent getEvent_id() {
		return event_id;
	}

	public void setEvent_id(LoggingEvent event_id) {
		this.event_id = event_id;
	}

	public short getI() {
		return i;
	}

	public void setI(short i) {
		this.i = i;
	}

}
