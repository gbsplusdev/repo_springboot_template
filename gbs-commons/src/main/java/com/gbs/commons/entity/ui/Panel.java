package com.gbs.commons.entity.ui;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.gbs.commons.dao.Identifiable;

@Entity
@Table(name = "ui_panels")
public class Panel implements Identifiable<Long>, Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(nullable = false, length = 25)
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Version
	private int version;
	
	private String name;
	private Label label;
	
	private PanelDisplayStyle displayStyle;
	
	@OneToMany(mappedBy="id")
	@LazyCollection(LazyCollectionOption.FALSE)
	@OrderBy("ordinal asc")
	private List<Field> fields;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getVersion() {
		return version;
	}
	
	public void setVersion(int version) {
		this.version = version;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Label getLabel() {
		return label;
	}

	public void setLabel(Label label) {
		this.label = label;
	}

	public List<Field> getFields() {
		return fields;
	}

	public void setFields(List<Field> fields) {
		this.fields = fields;
	}

	public PanelDisplayStyle getDisplayStyle() {
		return displayStyle;
	}

	public void setDisplayStyle(PanelDisplayStyle displayStyle) {
		this.displayStyle = displayStyle;
	}	
	
}
