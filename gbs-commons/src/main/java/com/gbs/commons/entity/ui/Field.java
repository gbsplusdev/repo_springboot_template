package com.gbs.commons.entity.ui;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.gbs.commons.dao.Identifiable;

@Entity
@Table(name = "ui_fields")
public class Field implements Identifiable<Long>, Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(nullable = false, length = 25)
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Version
	private int version;
	
	private String name;
	private Label label;
	private String position;
	private FieldType fieldType;
	private DisplayType displayType;
	private String maxlength;
	private String displayWidth;
	private String formatMask;
	
	@OneToMany(mappedBy="id")
	@LazyCollection(LazyCollectionOption.FALSE)
	@OrderBy("ordinal asc")
	private List<FieldSecurity> security;
			
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getVersion() {
		return version;
	}
	
	public void setVersion(int version) {
		this.version = version;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Label getLabel() {
		return label;
	}

	public void setLabel(Label label) {
		this.label = label;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public FieldType getFieldType() {
		return fieldType;
	}

	public void setFieldType(FieldType fieldType) {
		this.fieldType = fieldType;
	}

	public DisplayType getDisplayType() {
		return displayType;
	}

	public void setDisplayType(DisplayType displayType) {
		this.displayType = displayType;
	}

	public String getMaxlength() {
		return maxlength;
	}

	public void setMaxlength(String maxlength) {
		this.maxlength = maxlength;
	}

	public String getDisplayWidth() {
		return displayWidth;
	}

	public void setDisplayWidth(String displayWidth) {
		this.displayWidth = displayWidth;
	}

	public String getFormatMask() {
		return formatMask;
	}

	public void setFormatMask(String formatMask) {
		this.formatMask = formatMask;
	}

	public List<FieldSecurity> getSecurity() {
		return security;
	}

	public void setSecurity(List<FieldSecurity> security) {
		this.security = security;
	}	
	
}
