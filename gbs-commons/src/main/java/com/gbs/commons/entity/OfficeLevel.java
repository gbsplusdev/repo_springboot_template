package com.gbs.commons.entity;

import java.util.ArrayList;
import java.util.List;

import com.gbs.commons.util.KeyValuePair;

public enum OfficeLevel implements KeyValuePairs{
	
	GLOBAL("Global Office"),
	REGION("Regional Office"),
	COUNTRY("Country"),
	PROVINCE("State or Province"),
	BRANCH("Branch"),
	LOCATION("Location");
	
	private String value;
	
	private OfficeLevel(String value){
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public List<KeyValuePair<String, String>> getKeyValuePairs() {
		List<KeyValuePair<String, String>> list = new ArrayList<KeyValuePair<String, String>>();
		for(OfficeLevel officeLevel : values()){
			KeyValuePair<String, String> kvp = new KeyValuePair<String, String>(officeLevel.name(), officeLevel.value);
			list.add(kvp);
		}
		return list;
	}
}
