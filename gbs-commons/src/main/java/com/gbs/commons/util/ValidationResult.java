package com.gbs.commons.util;

public class ValidationResult {
	private ValidationResultType type = null;
	private String message = null;
	public ValidationResult(){
		
	}
	public ValidationResult(ValidationResultType type, String message) {
		this.type = type;
		this.message = message;
	}

	public ValidationResultType getType() {
		return type;
	}

	public void setType(ValidationResultType type) {
		this.type = type;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public String getDisplayMessage(){
		return type.getText() + " " + message;
	}

	public enum ValidationResultType{
		INFO{
			public String getText(){
				return "";
			}
		},
		WARNING{
			public String getText(){
				return "";
			}
		},
		ERROR{
			public String getText(){
				return "";
			}
		},
		ALERT{
			public String getText(){
				return "";
			}
		};
		public abstract String getText();
	}
}
