package com.gbs.commons.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import com.gbs.commons.util.ValidationResult.ValidationResultType;

public class SaveResult {
	private List<ValidationResult> validationResultList = new ArrayList<ValidationResult>();
	private Object savedObject;
	
	public List<ValidationResult> getValidationResultList() {
		return validationResultList;
	}

	public void setValidationResultList(List<ValidationResult> validationResultList) {
		this.validationResultList = validationResultList;
	}
	public void addValidationResultList(List<ValidationResult> vrl) {
		if(validationResultList == null){
			validationResultList = new ArrayList<ValidationResult>();
		}
		if(vrl == null){
			vrl = new ArrayList<ValidationResult>();
		}
		for(Iterator<ValidationResult> inVrItr = vrl.iterator(); inVrItr.hasNext();){
			ValidationResult inVr = inVrItr.next();
			boolean entryPresent = false;
			for(ValidationResult vr : validationResultList){
				if(inVr.getType() == vr.getType() && inVr.getMessage().equals(vr.getMessage())){
					//inVrItr.remove();
					entryPresent = true;
					break;
				}
				
			}
			if(!entryPresent){
				validationResultList.add(inVr);
			}
		}
		//validationResultList.addAll(vrl);
	}
	public void addValidationResult(ValidationResult vr) {
		if(validationResultList == null){
			validationResultList = new ArrayList<ValidationResult>();
		}
		addValidationResultList(Collections.singletonList(vr));
	}
	
	public void addValidationError(String error) {
		addValidationResult(new ValidationResult(ValidationResultType.ERROR, error));
	}
	
	public void addValidationAlert(String alert) {
		addValidationResult(new ValidationResult(ValidationResultType.ALERT, alert));
	}
	
	public Object getSavedObject() {
		return savedObject;
	}

	public void setSavedObject(Object savedObject) {
		this.savedObject = savedObject;
	}
	
	public boolean isReadyToSave(){
		return validationResultList == null || validationResultList.isEmpty();
	}

	public void addValidationResults(SaveResult save) {
		addValidationResultList(save.validationResultList);		
	}
	
	public ValidationResult getAlert(){
		for(ValidationResult vr : validationResultList){
			if(vr.getType() == ValidationResultType.ALERT){
				return vr;
			}
		}
		return null;
	}
	public List<ValidationResult> getNonAlerts(){
		List<ValidationResult> list = new ArrayList<ValidationResult>();
		for(ValidationResult vr : validationResultList){
			if(vr.getType() != ValidationResultType.ALERT){
				list.add(vr);
			}
		}
		return list;
	}
}
