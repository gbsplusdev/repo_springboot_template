package com.gbs.commons.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PasswordUtil {
 
	private static final Logger logger = LoggerFactory.getLogger(PasswordUtil.class);
    private static final String CHAR_LIST = 
        "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890+*&#@$";
    String randomString = "";
    private static final String UpperCaseList = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final String LowerCaseList = "abcdefghijklmnopqrstuvwxyz";
    private static final String NumericList ="1234567890";
    private static final String NonAlphaNumericList = "+*&#@$";     
     
    /**
     * This method generates random string
     * @return
     */
    
    public String generateRandomString(int maxLength,int minLength,int noOfLower,int noOfUpper,int noOfNumeric,int noOfNonAlphaNumeric){
         
    	int currentLenth=noOfLower+noOfUpper+noOfNumeric+noOfNonAlphaNumeric;
        StringBuffer randStr = new StringBuffer();
        randStr.append(generateRandomStrings(UpperCaseList,noOfUpper));
        randStr.append(generateRandomStrings(LowerCaseList,noOfLower));
        randStr.append(generateRandomStrings(NumericList,noOfNumeric));
       
        randStr.append(generateRandomStrings(NonAlphaNumericList,noOfNonAlphaNumeric));
        randStr.append(generateRandomStrings(CHAR_LIST,maxLength-currentLenth));     
        logger.debug(Integer.toString(maxLength));
       
       randomString=shuffle(randStr.toString());
        return randomString;
        
        
    }
     
    private String generateRandomStrings(String characterList,int minLength) {    	
        StringBuffer randStr = new StringBuffer();
    	for(int i=0; i<minLength; i++){
            int number = getRandomNumber(characterList.length());
            char ch = characterList.charAt(number);
            randStr.append(ch);
        }
    	return randStr.toString();
    }    
     /**
     * This method generates random numbers
     * @return int
     */
    
    private int getRandomNumber(int lenth) {
        int randomInt = 0;
        Random randomGenerator = new Random();
        randomInt = randomGenerator.nextInt(lenth);
        if (randomInt - 1 == -1) {
            return randomInt;
        } else {
            return randomInt - 1;
        }
    }    
    
    
    public boolean ValidatePassword(String password,int maxLength,int minLength,int noOfLower,int noOfUpper,int noOfNumeric,int noOfNonAlphaNumeric) {
    	int vnoOfLower=0,vnoOfUpper=0,vnoOfNumeric=0,vnoOfNonAlphaNumeric=0;
		
    	if(password.length()>maxLength||password.length()<minLength)
    	return false;    	
    	for (char p : password.toCharArray()) { 
			
			if(UpperCaseList.indexOf(p)>=0)
			{
				vnoOfUpper++;
			}
			else if(LowerCaseList.indexOf(p)>=0)
			{
				vnoOfLower++;
			} 
			else if(NumericList.indexOf(p)>=0)
			{
				vnoOfNumeric++;
			} 
			else if(NonAlphaNumericList.indexOf(p)>=0)
			{
				vnoOfNonAlphaNumeric++;
			} 							
		}
    	System.out.println(vnoOfUpper+"\t"+vnoOfLower+"\t"+vnoOfNumeric+"\t"+vnoOfNonAlphaNumeric);
    	if ((vnoOfLower >=noOfLower) && (vnoOfUpper>=noOfUpper) &&(vnoOfNumeric>=noOfNumeric) && (vnoOfNonAlphaNumeric>=noOfNonAlphaNumeric) ) 
		return true;	
		
		return false;
	}
    
    public String shuffle(String input){
        List<Character> characters = new ArrayList<Character>();
        for(char c:input.toCharArray()){
            characters.add(c);
        }
        StringBuilder output = new StringBuilder(input.length());
        while(characters.size()!=0){
            int randPicker = (int)(Math.random()*characters.size());
            output.append(characters.remove(randPicker));
        }
        return output.toString();
    }
    
}