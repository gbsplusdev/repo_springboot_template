package com.gbs.commons.util;

import java.io.Serializable;

public class KeyValuePair<K,V> implements Serializable {

	private static final long serialVersionUID = 6009373675284211479L;
	
	private K key		= null;
	private V value 	= null;
	
	public K getKey() {
		return key;
	}
	public void setKey(K key) {
		this.key = key;
	}
	public V getValue() {
		return value;
	}
	public void setValue(V value) {
		this.value = value;
	}	
	public KeyValuePair(K key, V value) {
		super();
		this.key = key;
		this.value = value;
	}	
	public KeyValuePair() {
		super();
	}
}
