package com.gbs.commons.util;

import java.util.ArrayList;
import java.util.List;

import com.gbs.commons.entity.KeyValuePairs;

public enum Week implements KeyValuePairs{
	
	MONDAY("Monday"), TUESDAY("Tuesday"), WEDNESDAY("Wednesday"), THURSDAY("Thursday"), FRIDAY("Friday"),
	SATURDAY("Saturday"), SUNDAY("Sunday");
	
	public String value;
	
	private Week(String value){
		this.value = value;
	}
	
	public String getValue(){
		return value;
	}

	//@Override
	public List<KeyValuePair<String, String>> getKeyValuePairs() {
		List<KeyValuePair<String, String>> list = new ArrayList<KeyValuePair<String, String>>();
		for(Week value : values()){
			KeyValuePair<String, String> kvp = new KeyValuePair<String, String>(value.name(), value.value);
			list.add(kvp);
		}
		return list;
	}	
}
