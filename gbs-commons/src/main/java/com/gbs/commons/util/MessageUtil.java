package com.gbs.commons.util;

import java.util.HashMap;

import com.gbs.commons.entity.Message;

public class MessageUtil {

	private static MessageUtil me;
	private static HashMap<String, Message> messages;

	private MessageUtil() {
		initialize();
	}
	
	private void initialize(){
		messages = new HashMap<String, Message> ();
		// Need to cache the messages in the hashmap;
	}

	public static MessageUtil getInstance() {
		
		// Need to introduce double checked locking if needed, to avoid duplicate instance creation
		if (me == null) { 
			me = new MessageUtil();			
		}
		return me;
	}
	
	public Message getMessage( String messageId, String groupId, Long languageId  ){
		
		String messageKey = languageId + groupId + messageId;
		Message message = messages.get(messageKey);
//		LanguageService languageService;
//		Language parentLanguage;
//		
//		if( message == null ){
//			languageService = new LanguageService(); //auto-wiring should be used...
//			parentLanguage = languageService.getParentLanguage(languageId);
//			messageKey = parentLanguage.getId() + groupId + messageId;
//			message = messages.get(messageKey);
//		}
		return message;
	}
}
