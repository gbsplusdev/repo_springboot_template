package com.gbs.commons.util;

import com.gbs.commons.entity.User;

public class LoginContext {
	
	private User user;
	
	public LoginContext(User user) {
		this.user = user;
	}
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}	
}
