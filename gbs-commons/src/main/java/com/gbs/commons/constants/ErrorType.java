/*--------------------------------------------------------------------------------------------------------------
AUTHOR                          : Yogesh H Shenoy
DATE                            : 31/01/2018
PURPOSE/NOTES                   : Error type for response errors
IMPORTANT VARIABLES             :
ASSUMPTIONS/LIMITATIONS         :
GLOBAL VARIABLES USED           :
LIBRARY USED                    :  
CLASSES USED                    : 
----------------------------------------------------------------------------------------------------------------
REVISION HISTORY
----------------------------------------------------------------------------------------------------------------
Date                            By                                      Notes
----------------------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------------------
PREFIX OF VARIABLES
GLOBAL                          : g_
CLASS                           : _
SUB/FUNCTION/PROPERTY/EVENT     : NIL
ARGUMENTS                       : a_ 
--------------------------------------------------------------------------------------------------------------*/

package com.gbs.commons.constants;


/** 
 * enum for error type
 * 
 */
public enum ErrorType {

	ERROR, WARNING, INFO
}
