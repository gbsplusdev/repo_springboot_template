package com.gbs.commons.constants;
/*
 * Constants class test
 */
public class Constants {
	public static final String DATE_FORMAT = "dd/MM/yyyy";
	public static final String DATE_TIME_FORMAT = "dd/MM/yyyy hh:mm a";
	public static final String LOW_DATE = "01/01/1900";
	public static final String HIGH_DATE = "31/12/2099";

	// Number Constants
	public static final double HIGH_AMOUNT_VALUE = 999999999;
	public static final double LOW_AMOUNT_VALUE = -999999999;

	// Encryption Constants
	public static final String DIGEST_ALGORITHM = "MD5";
	public static final String SALT_VAL = "Cl@zzT53 S5curity S@lt k5y";

	public static final String ERROR_PREFIX = "";
	public static final String SUCCESS_PREFIX = "";
	public static final String WARNING_PREFIX = "";

	public static final String TRAVERSE_PREVIOUS = "prev";
	public static final String TRAVERSE_FIRST = "first";
	public static final String TRAVERSE_NEXT = "next";
	public static final String TRAVERSE_LAST = "last";

	public static final int BATCH_SIZE = 10;

	public static final int MIN_FRACTION_SIZE = 3;
	public static final int MAX_FRACTION_SIZE = 3;

	public static final String QUERY_ALIAS = "12345";

}
