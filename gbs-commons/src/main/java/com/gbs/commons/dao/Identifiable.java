package com.gbs.commons.dao;

import java.io.Serializable;

public interface Identifiable<T> extends Serializable {
	public T getId(); 
	public int getVersion();
}
