/*--------------------------------------------------------------------------------------------------------------
AUTHOR                          : Yogesh H Shenoy
DATE                            : 31/01/2018
PURPOSE/NOTES                   : Base entity
IMPORTANT VARIABLES             :
ASSUMPTIONS/LIMITATIONS         :
GLOBAL VARIABLES USED           :
LIBRARY USED                    :  
CLASSES USED                    : 
----------------------------------------------------------------------------------------------------------------
REVISION HISTORY
----------------------------------------------------------------------------------------------------------------
Date                            By                                      Notes
----------------------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------------------
PREFIX OF VARIABLES
GLOBAL                          : g_
CLASS                           : _
SUB/FUNCTION/PROPERTY/EVENT     : NIL
ARGUMENTS                       : a_ 
--------------------------------------------------------------------------------------------------------------*/

package com.gbs.commons.model;

import com.gbs.commons.constants.Mode;

/**
 * Base entity
 * 
 * @param <E>
 *            entity type
 * 
 */
public abstract class AbstractEntity<E> {

	/**
	 * This validation function can must be overridden to implement customized
	 * validation. User the mode to apply different validation depending on
	 * option or functionality.
	 * 
	 * @param entity
	 * @param mode
	 * @return Response
	 */
	public abstract Response<?> validate(E entity, Mode mode);

	/**
	 * toString must be overridden by all entity. This function must return ID
	 * of the entity as string. In case of composite key all composite key
	 * attributes must be concatenated in string format.
	 * 
	 * @return string
	 */
	public abstract String getPrimaryKey();

}
