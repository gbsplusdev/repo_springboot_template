/*--------------------------------------------------------------------------------------------------------------
AUTHOR                          : Yogesh H Shenoy
DATE                            : 31/01/2018
PURPOSE/NOTES                   : entity for error inside response
IMPORTANT VARIABLES             :
ASSUMPTIONS/LIMITATIONS         :
GLOBAL VARIABLES USED           :
LIBRARY USED                    : querydsl 
CLASSES USED                    : PagingSortingRepository, QueryDslPredicateExecutor
----------------------------------------------------------------------------------------------------------------
REVISION HISTORY
----------------------------------------------------------------------------------------------------------------
Date                            By                                      Notes
----------------------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------------------
PREFIX OF VARIABLES
GLOBAL                          : g_
CLASS                           : _
SUB/FUNCTION/PROPERTY/EVENT     : NIL
ARGUMENTS                       : a_ 
--------------------------------------------------------------------------------------------------------------*/

package com.gbs.commons.model;

import com.gbs.commons.constants.ErrorType;


/**
 * Common error
 *
 */
public class Error {

	/**
	 * Error code This error code must be unique throughout the project Standard
	 * for identifying error using error code {coming soon}
	 */
	private String code;
	/**
	 * Short description about the error occurred
	 */
	private String message;
	/**
	 * Error type Possible values: error, warning and info
	 */
	private ErrorType errorType;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public ErrorType getTypes() {
		return errorType;
	}

	public void setTypes(ErrorType types) {
		this.errorType = types;
	}

}
