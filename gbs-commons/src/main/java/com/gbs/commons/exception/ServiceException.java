/*--------------------------------------------------------------------------------------------------------------
AUTHOR                          : Yogesh H Shenoy
DATE                            : 31/01/2018
PURPOSE/NOTES                   : Custom service exception
IMPORTANT VARIABLES             :
ASSUMPTIONS/LIMITATIONS         :
GLOBAL VARIABLES USED           :
LIBRARY USED                    :  
CLASSES USED                    : Exception
----------------------------------------------------------------------------------------------------------------
REVISION HISTORY
----------------------------------------------------------------------------------------------------------------
Date                            By                                      Notes
----------------------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------------------
PREFIX OF VARIABLES
GLOBAL                          : g_
CLASS                           : _
SUB/FUNCTION/PROPERTY/EVENT     : NIL
ARGUMENTS                       : a_ 
--------------------------------------------------------------------------------------------------------------*/

package com.gbs.commons.exception;

import com.gbs.commons.model.Response;

/**
 * Custom ServiceException, all exceptions caught inside service layer will be
 * thrown as ServiceException
 * 
 */
public class ServiceException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Response property is used, to pass the response through the exception
	 * caught inside the service.
	 */
	private Response<?> response;

	/**
	 * Constructor for setting custom exception message
	 * 
	 * @param message
	 * @param response
	 */
	public ServiceException(String message, Response<?> response) {
		super(message);
		this.setResponse(response);
	}

	/**
	 * Constructor for setting custom exception cause
	 * 
	 * @param cause
	 * @param response
	 */
	public ServiceException(Throwable cause, Response<?> response) {
		super(cause);
		this.setResponse(response);
	}

	/**
	 * Constructor for setting custom exception message and cause
	 * 
	 * @param message
	 * @param cause
	 * @param response
	 */
	public ServiceException(String message, Throwable cause, Response<?> response) {
		super(message, cause);
		this.setResponse(response);
	}

	public Response<?> getResponse() {
		return response;
	}

	public void setResponse(Response<?> response) {
		this.response = response;
	}
}
