/*--------------------------------------------------------------------------------------------------------------
AUTHOR                          : Yogesh H Shenoy
DATE                            : 31/01/2018
PURPOSE/NOTES                   : Custom database exception
IMPORTANT VARIABLES             :
ASSUMPTIONS/LIMITATIONS         :
GLOBAL VARIABLES USED           :
LIBRARY USED                    :  
CLASSES USED                    : Exception
----------------------------------------------------------------------------------------------------------------
REVISION HISTORY
----------------------------------------------------------------------------------------------------------------
Date                            By                                      Notes
----------------------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------------------
PREFIX OF VARIABLES
GLOBAL                          : g_
CLASS                           : _
SUB/FUNCTION/PROPERTY/EVENT     : NIL
ARGUMENTS                       : a_ 
--------------------------------------------------------------------------------------------------------------*/

package com.gbs.commons.exception;

import com.gbs.commons.model.Response;
/**
 * Custom database exception. Used for throwing database related exceptions. e.g. JDBCConnectionException, HibernateQueryException etc.
 * 
 * @author Yogesh
 *
 */
public class DataBaseException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Response property is used, to pass the response through the exception
	 * caught inside the service.
	 */
	private Response<?> response;

	/**
	 * Constructor for setting custom exception message
	 * 
	 * @param message
	 * @param response
	 */
	public DataBaseException(String message, Response<?> response) {
		super(message);
		this.setResponse(response);
	}

	/**
	 * Constructor for setting custom exception cause
	 * 
	 * @param cause
	 * @param response
	 */
	public DataBaseException(Throwable cause, Response<?> response) {
		super(cause);
		this.setResponse(response);
	}

	/**
	 * Constructor for setting custom exception message and cause
	 * 
	 * @param message
	 * @param cause
	 * @param response
	 */
	public DataBaseException(String message, Response<?> response, Throwable cause) {
		super(message, cause);
		this.setResponse(response);
	}

	public Response<?> getResponse() {
		return response;
	}

	public void setResponse(Response<?> response) {
		this.response = response;
	}
}
