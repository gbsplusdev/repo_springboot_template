package com.gbs.commons.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.querydsl.core.types.Predicate;

import com.gbs.commons.constants.Mode;
import com.gbs.commons.entity.Office;
import com.gbs.commons.model.FilterCondition;
import com.gbs.commons.model.Response;
import com.gbs.commons.repository.OfficeRepository;

@Service
public class OfficeService  extends AbstractService<Office,Long>{

	@Autowired
	private OfficeRepository officeRepository;
	
	public boolean deleteAll() {
		try {
			officeRepository.deleteAll();
			return true;	
		} catch (Exception e) {
		}
		return false;
	}
	@Override
	protected Predicate createPredicate(List<FilterCondition> filterConditions) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Boolean beValidate(Office entity, Response<?> response, Mode mode) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	protected Boolean bePopulate(Office entity, Response<?> response, Mode mode) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	protected Boolean businessValidate(Office entity, Response<?> response, Mode mode) {
		// TODO Auto-generated method stub
		return true;
	}

}
