package com.gbs.commons.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.querydsl.core.types.Predicate;

import com.gbs.commons.repository.RoleRepository;
import com.gbs.commons.constants.Mode;
import com.gbs.commons.entity.Role;
import com.gbs.commons.model.FilterCondition;
import com.gbs.commons.model.Response;

@Service
public class RoleService extends AbstractService<Role, Long>{

	@Autowired
	private RoleRepository roleRepository;
	
	public boolean deleteAll() {
		try {
			roleRepository.deleteAll();
			return true;	
		} catch (Exception e) {
		}
		return false;
	}

	@Override
	protected Predicate createPredicate(List<FilterCondition> filterConditions) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Boolean beValidate(Role entity, Response<?> response, Mode mode) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	protected Boolean bePopulate(Role entity, Response<?> response, Mode mode) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	protected Boolean businessValidate(Role entity, Response<?> response, Mode mode) {
		// TODO Auto-generated method stub
		return true;
	}
		
}
