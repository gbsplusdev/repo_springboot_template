package com.gbs.commons.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.gbs.commons.entity.User;
import com.gbs.commons.repository.UserRepository;
import com.gbs.commons.util.LoginContext;
import com.gbs.commons.util.SaveResult;
import com.gbs.commons.util.ValidationResult;

@Service
public class UserService implements Serviceable<Long, User> {

	@Autowired
	private UserRepository userRepository;

	// @Autowired
	// private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Override
	public List<ValidationResult> doValidate(User obj) {
		return null;
	}

	@Override
	public User findById(Long id) {
		return userRepository.findOne(id);
	}

	@Override
	public SaveResult save(User user) {
		// user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		user.setisActivated(true);
		SaveResult result = new SaveResult();
		result.setSavedObject(userRepository.save(user));
		return null;
	}

	@Override
	public SaveResult save(LoginContext context, User obj) {
		return null;
	}

	@Override
	public SaveResult afterSave(User obj) {
		return null;
	}

	@Override
	public SaveResult delete(Long id) {

		userRepository.delete(id);
		return null;
	}

	@Override
	public List<User> getPaginatedList() {
		return null;
	}

	public User findUserByEmail(String email) {
		User user = userRepository.findByEmail(email);
		return user;
	}

	public User findByUserName(String username) {
		User user = userRepository.findByUserName(username);
		return user;
	}

	public User Save(User user) {

		return userRepository.save(user);
	}

	public User Update(User user) {
		return userRepository.save(user);
	}

	public Page<User> getAll(Pageable pageable) {

		return userRepository.findAll(pageable);
	}

}
