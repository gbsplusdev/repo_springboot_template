package com.gbs.commons.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.querydsl.core.types.Predicate;
import com.gbs.commons.constants.Mode;
import com.gbs.commons.entity.MenuGroup;
import com.gbs.commons.model.FilterCondition;
import com.gbs.commons.model.Response;
import com.gbs.commons.repository.MenuGroupRepository;

@Service
public class MenuGroupService extends AbstractService<MenuGroup, Long> {

	
	@Autowired
	private MenuGroupRepository menuGroupRepository;
	
	public boolean deleteAll() {
		try {
			menuGroupRepository.deleteAll();
			return true;	
		} catch (Exception e) {
		}
		return false;
	}
	@Override
	protected Predicate createPredicate(List<FilterCondition> filterConditions) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Boolean beValidate(MenuGroup entity, Response<?> response, Mode mode) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	protected Boolean bePopulate(MenuGroup entity, Response<?> response, Mode mode) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	protected Boolean businessValidate(MenuGroup entity, Response<?> response, Mode mode) {
		// TODO Auto-generated method stub
		return true;
	}
	
	
	
}
