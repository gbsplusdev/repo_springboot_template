package com.gbs.commons.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.querydsl.core.types.Predicate;

import com.gbs.commons.constants.Mode;
import com.gbs.commons.entity.MenuConfig;
import com.gbs.commons.model.FilterCondition;
import com.gbs.commons.model.Response;
import com.gbs.commons.repository.MenuConfigRepository;

@Service
public class MenuConfigService extends AbstractService<MenuConfig,Long>{

	@Autowired
	private MenuConfigRepository menuConfigRepository;

	public boolean deleteAll() {
		try {
			menuConfigRepository.deleteAll();
			return true;	
		} catch (Exception e) {
		}
		return false;
	}
	@Override		
	protected Predicate createPredicate(List<FilterCondition> filterConditions) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Boolean beValidate(MenuConfig entity, Response<?> response, Mode mode) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	protected Boolean bePopulate(MenuConfig entity, Response<?> response, Mode mode) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	protected Boolean businessValidate(MenuConfig entity, Response<?> response, Mode mode) {
		// TODO Auto-generated method stub
		return true;
	}

		
}
