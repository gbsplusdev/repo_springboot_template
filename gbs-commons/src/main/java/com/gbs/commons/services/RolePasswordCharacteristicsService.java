package com.gbs.commons.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.querydsl.core.types.Predicate;

import com.gbs.commons.constants.Mode;
import com.gbs.commons.entity.RolePasswordCharacteristics;
import com.gbs.commons.model.FilterCondition;
import com.gbs.commons.model.Response;
import com.gbs.commons.repository.RolePasswordCharacteristicsRepository;
@Service
public class RolePasswordCharacteristicsService extends AbstractService<RolePasswordCharacteristics, Long> {

	@Autowired
	private RolePasswordCharacteristicsRepository rolePasswordCharacteristicsRepository;

	public RolePasswordCharacteristics findByRoleId(long roleId) {
		return rolePasswordCharacteristicsRepository.findByRoleId(roleId);
	}

	public boolean deleteAll() {
		try {
			rolePasswordCharacteristicsRepository.deleteAll();
			return true;	
		} catch (Exception e) {
		}
		return false;
	}
	@Override
	protected Predicate createPredicate(List<FilterCondition> filterConditions) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Boolean beValidate(RolePasswordCharacteristics entity, Response<?> response, Mode mode) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	protected Boolean bePopulate(RolePasswordCharacteristics entity, Response<?> response, Mode mode) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	protected Boolean businessValidate(RolePasswordCharacteristics entity, Response<?> response, Mode mode) {
		// TODO Auto-generated method stub
		return true;
	}	  	  	  		
}
