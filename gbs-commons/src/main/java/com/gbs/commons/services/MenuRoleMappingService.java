package com.gbs.commons.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.querydsl.core.types.Predicate;

import com.gbs.commons.constants.Mode;
import com.gbs.commons.entity.MenuRoleMapping;
import com.gbs.commons.model.FilterCondition;
import com.gbs.commons.model.Response;
import com.gbs.commons.repository.MenuRoleMappingRepository;
@Service
public class MenuRoleMappingService extends AbstractService<MenuRoleMapping, Long> {

	@Autowired
	private MenuRoleMappingRepository menuRoleMappingRepository;
	
	public boolean deleteAll() {
		try {
			menuRoleMappingRepository.deleteAll();
			return true;	
		} catch (Exception e) {
		}
		return false;
	}
	@Override
	protected Predicate createPredicate(List<FilterCondition> filterConditions) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Boolean beValidate(MenuRoleMapping entity, Response<?> response, Mode mode) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	protected Boolean bePopulate(MenuRoleMapping entity, Response<?> response, Mode mode) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	protected Boolean businessValidate(MenuRoleMapping entity, Response<?> response, Mode mode) {
		// TODO Auto-generated method stub
		return true;
	}

}
