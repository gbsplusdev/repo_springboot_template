package com.gbs.commons.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.gbs.commons.entity.Language;
import com.gbs.commons.repository.LanguageRepository;
import com.gbs.commons.services.Serviceable;
import com.gbs.commons.util.LoginContext;
import com.gbs.commons.util.SaveResult;
import com.gbs.commons.util.ValidationResult;

@Service
public class LanguageService implements Serviceable<Long, Language> {

	@Autowired
	private LanguageRepository languageRepository;

	@Override
	public List<ValidationResult> doValidate(Language obj) {
		return null;
	}

	@Override
	public Language findById(Long id) {
		return null;
	}

	public Language findByDescription(String description) {
		return languageRepository.findByDescription(description);
	}

	@Override
	public SaveResult save(Language language) {
		SaveResult result = new SaveResult();
		result.setSavedObject(languageRepository.save(language));
		return result;
	}

	@Override
	public SaveResult save(LoginContext context, Language obj) {
		return null;
	}

	@Override
	public SaveResult afterSave(Language obj) {
		return null;
	}

	@Override
	public SaveResult delete(Long id) {
		return null;
	}

	@Override
	public List<Language> getPaginatedList() {
		return null;
	}

	public Language getParentLanguage(Long id) {
		return null;
	}

	public Language Save(Language language) {

		return languageRepository.save(language);
	}

	public Language Update(Language language) {

		return languageRepository.save(language);
	}

	public Language Delete(Long id) {
		Language language = languageRepository.findOne(id);
		languageRepository.delete(language);
		return language;
	}

	public Language FindByID(Long id) {

		return languageRepository.findOne(id);
	}

	public Page<Language> getAll(Pageable pageable) {

		return languageRepository.findAll(pageable);
	}

	public Language findOne(Long languageId) {

		return languageRepository.findOne(languageId);
	}

}
