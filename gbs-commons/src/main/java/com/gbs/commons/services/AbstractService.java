/*--------------------------------------------------------------------------------------------------------------
AUTHOR                          : Yogesh H Shenoy
DATE                            : 31/01/2018
PURPOSE/NOTES                   : Base service
IMPORTANT VARIABLES             :
ASSUMPTIONS/LIMITATIONS         :
GLOBAL VARIABLES USED           :
LIBRARY USED                    :  
CLASSES USED                    : 
----------------------------------------------------------------------------------------------------------------
REVISION HISTORY
----------------------------------------------------------------------------------------------------------------
Date                            By                                      Notes
--------------------------------------------------------------------------------------------------------------*/

package com.gbs.commons.services;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.jdo.annotations.Transactional;

import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.gbs.commons.model.Error;
import com.gbs.commons.constants.ErrorType;
import com.gbs.commons.constants.Mode;
import com.gbs.commons.exception.DataBaseException;
import com.gbs.commons.model.CacheSave;
import com.gbs.commons.model.FilterCondition;
import com.gbs.commons.model.AbstractEntity;
import com.gbs.commons.model.Response;
import com.gbs.commons.repository.AbstractRepository;
import com.gbs.commons.util.CommonUtils;
import com.querydsl.core.types.Predicate;

/**
 * Base class for service
 * 
 * @param <E>
 * @param <I>
 */
public abstract class AbstractService<E extends AbstractEntity<E>, I extends Serializable> {

	final static Logger logger = LoggerFactory.getLogger(AbstractService.class);

	/**
	 * Performs save operation for the service. The data can only be saved after
	 * passing through pre process and then business logic validations
	 * 
	 * @param entity
	 * @param muzirisAbstractRepository
	 * @return response
	 * @throws DataBaseException
	 */
	@Transactional
	public final Response<E> save(E entity, AbstractRepository<E, I> muzirisAbstractRepository)
			throws DataBaseException {
		logger.debug("save started");
		Response<E> response = new Response<E>();
		try {
			if (!preProcess(entity, response, Mode.INSERT)) {
				response.setSuccess(false);
			}
			if (!businessValidate(entity, response, Mode.INSERT)) {
				response.setSuccess(false);
			} else {
				response.setData(executeRepositorySave(entity, muzirisAbstractRepository));
				response.setSuccess(true);
			}
		} catch (HibernateException hibernateException) {
			setResponseOnException(response);
			logger.error(hibernateException.toString());
			throw new DataBaseException("Internal database server error", response);
		} finally {
		}
		logger.debug("save completed");
		return response;
	}

	/**
	 * Performs update operation for the service. The data can only be updated
	 * after passing through pre process and then business logic validations.
	 * Also before updating it must be ensured that the entity is locked by the
	 * current user to be able to successful update data. After updating the
	 * data the lock on the data must be released so that other users will be
	 * able to update the same data.
	 * 
	 * @param entity
	 * @param muzirisAbstractRepository
	 * @return response
	 * @throws DataBaseException
	 */
	@Transactional
	public final Response<E> update(E entity, AbstractRepository<E, I> muzirisAbstractRepository, Long userId)
			throws DataBaseException {
		logger.debug("update started");
		Response<E> response = new Response<E>();
		try {
			if (!preProcess(entity, response, Mode.UPDATE)) {
				response.setSuccess(false);
			}
			if (!businessValidate(entity, response, Mode.UPDATE)) {
				response.setSuccess(false);
			}
			/*
			 * if (!checkForLock(entity, response, userId)) {
			 * response.setSuccess(false); }
			 */ else {
				response.setData(executeRepositorySave(entity, muzirisAbstractRepository));
				response.setSuccess(true);
				// removeLock(entity, response, userId);
			}
		} catch (HibernateException hibernateException) {
			setResponseOnException(response);
			logger.error(hibernateException.toString());
			throw new DataBaseException("Internal database server error", response);
		} finally {
		}
		logger.debug("update completed");
		return response;
	}

	/**
	 * Performs delete operation for the service. The data can only be deleted
	 * after going through pre process and business logic validations
	 * 
	 * @param entity
	 * @param muzirisAbstractRepository
	 * @return response
	 * @throws DataBaseException
	 */
	@Transactional
	public final Response<E> delete(E entity, AbstractRepository<E, I> muzirisAbstractRepository)
			throws DataBaseException {
		logger.debug("delete started");
		Response<E> response = new Response<E>();
		try {
			if (!preProcess(entity, response, Mode.DELETE)) {
				response.setData(entity);
			}
			if (!businessValidate(entity, response, Mode.DELETE)) {
				response.setData(entity);
			} else {
				executeRepositoryDelete(entity, muzirisAbstractRepository);
				response.setData(entity);
			}
		} catch (HibernateException hibernateException) {
			setResponseOnException(response);
			logger.error(hibernateException.toString());
			throw new DataBaseException("Internal database server error", response);
		} finally {

		}
		logger.debug("delete completed");
		return response;
	}

	/**
	 * To fetch pageable data
	 * 
	 * @param index
	 * @param size
	 * @param conditionlist
	 * @param muzirisAbstractRepository
	 * @return response
	 * @throws DataBaseException
	 */
	public final Response<Object> getPaginatedList(Integer index, Integer size, List<FilterCondition> filterConditions,
			AbstractRepository<E, I> muzirisAbstractRepository) throws DataBaseException {
		Response<Object> response = new Response<Object>();
		logger.debug("getPaginatedList started");
		try {
			Pageable pagable = new PageRequest(index, size);

			if (!filterConditions.isEmpty()) {

				response.setData(muzirisAbstractRepository.findAll(createPredicate(filterConditions), pagable));

			} else {
				response.setData(muzirisAbstractRepository.findAll(pagable));
			}
		} catch (HibernateException hibernateException) {
			setResponseOnException(response);
			response.setData(null);
			logger.error(hibernateException.toString());
			throw new DataBaseException("Internal database server error", response);
		} finally {
		}
		logger.debug("getPaginatedList completed");
		return response;
	}

	/**
	 * To fetch all data. If the MODE is passed as UPDATE locking will be
	 * applied to the data fetched.
	 * 
	 * @param filterConditions
	 * @param muzirisAbstractRepository
	 * @param mode
	 * @param customFieldsFlag
	 * @param userId
	 * @return
	 * @throws DataBaseException
	 */
	public final Response<List<E>> findAll(List<FilterCondition> filterConditions,
			AbstractRepository<E, I> muzirisAbstractRepository, Mode mode, String customFieldsFlag, Long userId)
			throws DataBaseException {
		Response<List<E>> response = new Response<List<E>>();
		logger.debug("findAll started");
		try {
			List<E> result;
			if (!filterConditions.isEmpty()) {
				if (customFieldsFlag == "") {
					result = CommonUtils.toList(muzirisAbstractRepository.findAll(createPredicate(filterConditions)));
					response.setData(result);
				} else {
					result = CommonUtils.toList(
							findAllWithCustomFields(filterConditions, muzirisAbstractRepository, customFieldsFlag));
					response.setData(result);
				}
			} else {
				result = CommonUtils.toList(muzirisAbstractRepository.findAll());
				response.setData(result);
			}
		} catch (HibernateException hibernateException) {
			setResponseOnException(response);
			response.setData(null);
			logger.error(hibernateException.toString());
			throw new DataBaseException("Internal database server error", response);
		} finally {
		}
		logger.debug("findAll completed");
		return response;
	}

	/**
	 * Fetches data based on the id. If the MODE is passed as UPDATE locking
	 * will be applied to the data fetched.
	 * 
	 * @param id
	 * @param muzirisAbstractRepository
	 * @param mode
	 * @param userId
	 * @return
	 * @throws DataBaseException
	 */
	public final Response<E> findOne(I id, AbstractRepository<E, I> muzirisAbstractRepository, Mode mode,
			Long userId) throws DataBaseException {

		Response<E> response = new Response<E>();
		logger.debug("findById started");
		try {
			response.setData(muzirisAbstractRepository.findOne(id));

			if (mode == Mode.UPDATE && !lockEntityForEdit((E) response.getData(), userId)) {
				response.setData(null);
			}

		} catch (HibernateException hibernateException) {
			setResponseOnException(response);
			response.setData(null);
			logger.error(hibernateException.toString());
			throw new DataBaseException("Internal database server error", response);
		} finally {
		}
		logger.debug("findById completed");
		return response;
	}

	/**
	 * To prepare response on exception from service
	 * 
	 * @param response
	 */
	private final void setResponseOnException(Response<?> response) {
		List<Error> errorList = new ArrayList<Error>();
		Error errors = new Error();
		errors.setCode("sopt10001");// error code will be modified when the
									// standard is defined
		errors.setMessage("Internal database server error");
		errors.setTypes(ErrorType.ERROR);
		errorList.add(errors);
		response.setErrors(errorList);
		response.setSuccess(false);
	}

	/**
	 * Overridable function for fetching data with required fields
	 * 
	 * @param filterConditions
	 * @param muzirisAbstractRepository
	 * @param customFieldsFlag
	 * @return
	 */
	private Iterable<E> findAllWithCustomFields(List<FilterCondition> filterConditions,
			AbstractRepository<E, I> muzirisAbstractRepository, String customFieldsFlag) {
		return muzirisAbstractRepository.findAll(createPredicate(filterConditions));
	}

	/**
	 * To predicate for filtering
	 * 
	 * @param filterConditions
	 * @return
	 * @throws ClassNotFoundException
	 */
	protected abstract Predicate createPredicate(List<FilterCondition> filterConditions);

	/**
	 * To perform pre processing. Two steps will be carried out beValidation -
	 * validating the entity and sub entities bePopulate - populating values for
	 * required properties in entity and sub entity (if required)
	 * 
	 * @param entity
	 * @param response
	 * @param mode
	 * @return true/false
	 */
	private final Boolean preProcess(E entity, Response<?> response, Mode mode) {
		logger.debug("preProcess started");
		if (!beValidate(entity, response, mode)) {
			logger.debug("preProcess completed");
			return false;
		}
		if (!bePopulate(entity, response, mode)) {
			logger.debug("preProcess completed");
			return false;
		} else {
			logger.debug("preProcess completed");
			return true;
		}
	}

	/**
	 * Validating the entity and sub entities
	 * 
	 * @param entity
	 * @param response
	 * @param mode
	 * @return
	 */
	protected abstract Boolean beValidate(E entity, Response<?> response, Mode mode);

	/**
	 * Populating values for required properties in entity and sub entity (if
	 * required)
	 * 
	 * @param entity
	 * @param response
	 * @param mode
	 * @return
	 */
	protected abstract Boolean bePopulate(E entity, Response<?> response, Mode mode);

	/**
	 * To perform business logic validations
	 * 
	 * @param entity
	 * @param response
	 * @param mode
	 * @return
	 */
	protected abstract Boolean businessValidate(E entity, Response<?> response, Mode mode);

	/**
	 * Execute repository call for save. If required this method can be
	 * overridden and redefined as per requirement
	 * 
	 * @param entity
	 * @param muzirisAbstractRepository
	 * @return
	 */
	private E executeRepositorySave(E entity, AbstractRepository<E, I> muzirisAbstractRepository) {
		logger.debug("executeRepositorySave started");
		entity = muzirisAbstractRepository.save(entity);
		logger.debug("executeRepositorySave completed");
		return entity;
	}

	/**
	 * Execute repository call for delete. If required this method can be
	 * overridden and redefined as per requirement
	 * 
	 * @param entity
	 * @param muzirisAbstractRepository
	 * @return
	 */
	private Boolean executeRepositoryDelete(E entity, AbstractRepository<E, I> muzirisAbstractRepository) {
		logger.debug("executeRepositoryDelete started");
		muzirisAbstractRepository.delete(entity);
		logger.debug("executeRepositoryDelete completed");
		return true;
	}

	/**
	 * To add fetched data to cache for optimistic locking to manage concurrency
	 * for single entity
	 * 
	 * @param entityForLock
	 *            uses data from the response for locking data
	 * @return true/false
	 */
	private final Boolean lockEntityForEdit(E entityForLock, Long userId) {
		String primaryKey, entityName, keyName;
		boolean returnValue;
		CacheService cacheService = new CacheService();
		List<Error> listErrors = new ArrayList<Error>();

		if (userId == 0) {
			Error errorData = new Error();
			errorData.setCode("sopt10001");// error code will be
											// modified when the
											// standard is defined
			errorData.setMessage("Invalid UserId");
			errorData.setTypes(ErrorType.ERROR);
			listErrors.add(errorData);
			returnValue = false;

		} else {
			primaryKey = entityForLock.getPrimaryKey();
			entityName = entityForLock.getClass().getName();
			keyName = entityName.concat(primaryKey);

			CacheSave cache = new CacheSave();
			cache.setLoginDate(new Date());
			cache.setKey(keyName);
			cache.setUserId(userId);
			cacheService.lockEntityForEdit(cache, listErrors);
			returnValue = true;
		}
		return returnValue;
	}

	/**
	 * To check weather the entity is locked by the current user. This function
	 * is executed every time before updating data. If the user do not have the
	 * lock on the data, then the user is not allowed to update it.
	 * 
	 * @param entity
	 * @param response
	 * @return
	 */
	@SuppressWarnings("unused")
	private final Boolean checkForLock(E entity, Response<?> response, Long userId) {
		String primaryKey, entityName, keyName;
		Boolean successOrFailure;
		CacheService cacheService = new CacheService();
		List<Error> listErrors = new ArrayList<Error>();
		if (userId == 0) {
			Error errorData = new Error();
			errorData.setCode("sopt10001");// error code will be
											// modified when the
											// standard is defined
			errorData.setMessage("Invalid UserId");
			errorData.setTypes(ErrorType.ERROR);
			listErrors.add(errorData);
			successOrFailure = false;

		} else {
			primaryKey = entity.getPrimaryKey();
			entityName = entity.getClass().getName();
			keyName = entityName.concat(primaryKey);

			successOrFailure = cacheService.checkForLock(keyName, listErrors, userId);

		}
		return successOrFailure;
	}

	/**
	 * To remove the lock applied on the data fetched for update. This function
	 * will be called every time after the update function is called.
	 * 
	 * @param entity
	 * @param response
	 * @return
	 */
	// This function is called only after the update operation is performed, so
	// if any error occurred in this function will have to be managed properly.
	// Otherwise the lock will not be released even after the data is updated.
	@SuppressWarnings("unused")
	private final Boolean removeLock(E entity, Response<?> response, Long userId) {
		String primaryKey, entityName, keyName;
		Boolean successOrFailure;
		CacheService cacheService = new CacheService();
		List<Error> listErrors = new ArrayList<Error>();
		if (userId == 0) {
			Error errorData = new Error();
			errorData.setCode("sopt10001");// error code will be
											// modified when the
											// standard is defined
			errorData.setMessage("Invalid UserId");
			errorData.setTypes(ErrorType.ERROR);
			listErrors.add(errorData);
			successOrFailure = false;
		} else {
			primaryKey = entity.getPrimaryKey();
			entityName = entity.getClass().getName();
			keyName = entityName.concat(primaryKey);

			successOrFailure = cacheService.removeLock(keyName, listErrors, userId);
		}
		return successOrFailure;
	}
}
