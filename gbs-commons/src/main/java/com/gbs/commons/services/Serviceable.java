package com.gbs.commons.services;

import java.util.List;

import com.gbs.commons.util.LoginContext;
import com.gbs.commons.util.SaveResult;
import com.gbs.commons.util.ValidationResult;

public interface Serviceable<K,V> {
	
	public List<ValidationResult> doValidate(V obj);
	public V findById(K id);
	public SaveResult save(V obj);
	public SaveResult save(LoginContext context, V obj);
	public SaveResult afterSave(V obj);
	public SaveResult delete(K id);
	public List<V> getPaginatedList();
}
	