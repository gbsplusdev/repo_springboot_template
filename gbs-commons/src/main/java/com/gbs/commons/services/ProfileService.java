package com.gbs.commons.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gbs.commons.entity.ui.Profile;
import com.gbs.commons.repository.ProfileRepository;
import com.gbs.commons.util.LoginContext;
import com.gbs.commons.util.SaveResult;
import com.gbs.commons.util.ValidationResult;
@Service
public class ProfileService implements Serviceable<Long, Profile>{
	
	@Autowired
	ProfileRepository profileRepository;

	@Override
	public List<ValidationResult> doValidate(Profile obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Profile findById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SaveResult save(Profile profile) {
		profile= profileRepository.save(profile);
		SaveResult result = new SaveResult();
		result.setSavedObject(profile);
		return result;
	}

	@Override
	public SaveResult save(LoginContext context, Profile profile) {
		profile= profileRepository.save(profile);
		SaveResult result = new SaveResult();
		result.setSavedObject(profile);
		return result;
	}

	@Override
	public SaveResult afterSave(Profile obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SaveResult delete(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Profile> getPaginatedList() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public Profile findByProfileName(String name) {
		return profileRepository.findByName(name);
	}

}
