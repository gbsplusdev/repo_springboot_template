package com.gbs.commons.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gbs.commons.entity.Message;
import com.gbs.commons.repository.MessageRepository;
import com.gbs.commons.util.LoginContext;
import com.gbs.commons.util.SaveResult;
import com.gbs.commons.util.ValidationResult;

@Service
public class MessageService implements Serviceable<Long, Message>{
	
	@Autowired
	private MessageRepository messageRepository;

	
	
	public Message findByMessageIdAndGroupId(String messageId,String groupId) {
		return messageRepository.findByMessageIdAndGroupId(messageId,groupId);
	}

	@Override
	public List<ValidationResult> doValidate(Message obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Message findById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SaveResult save(Message obj) {
		Message message = messageRepository.save(obj);
		SaveResult result = new SaveResult();
		result.setSavedObject(message);
		return result;
	}

	@Override
	public SaveResult save(LoginContext context, Message obj) {
		return null;
		
	}

	@Override
	public SaveResult afterSave(Message obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SaveResult delete(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Message> getPaginatedList() {
		// TODO Auto-generated method stub
		return null;
	}

}
