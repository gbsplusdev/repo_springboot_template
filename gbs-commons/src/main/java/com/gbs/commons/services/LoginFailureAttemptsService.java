package com.gbs.commons.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.querydsl.core.types.Predicate;

import com.gbs.commons.constants.Mode;
import com.gbs.commons.entity.LoginFailureAttempts;
import com.gbs.commons.model.FilterCondition;
import com.gbs.commons.model.Response;
import com.gbs.commons.repository.LoginFailureAttemptsRepository;

@Service
public class LoginFailureAttemptsService extends AbstractService<LoginFailureAttempts, Long> {

	@Autowired
	private LoginFailureAttemptsRepository loginFailureAttemptsRepository;
	
	public boolean deleteAll() {
		try {
			loginFailureAttemptsRepository.deleteAll();
			return true;	
		} catch (Exception e) {
		}
		return false;
	}
	
	
	
	public LoginFailureAttempts findByUserId(long id) {			
		
		LoginFailureAttempts loginFailureAttempt=loginFailureAttemptsRepository.findByUserId(id);
		if (loginFailureAttempt!=null)
			return loginFailureAttempt;
			
		else {
			LoginFailureAttempts newloginFailureAttempt=new LoginFailureAttempts();
			newloginFailureAttempt.setUserId(id);newloginFailureAttempt.setFailureAttempts(0);
			newloginFailureAttempt.setUserLocked(false);			
			 loginFailureAttemptsRepository.save(newloginFailureAttempt);
			return loginFailureAttemptsRepository.findByUserId(id);	 
		}							
		}
		


	@Override
	protected Predicate createPredicate(List<FilterCondition> filterConditions) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	protected Boolean beValidate(LoginFailureAttempts entity, Response<?> response, Mode mode) {
		// TODO Auto-generated method stub
		return true;
	}


	@Override
	protected Boolean bePopulate(LoginFailureAttempts entity, Response<?> response, Mode mode) {
		// TODO Auto-generated method stub
		return true;
	}


	@Override
	protected Boolean businessValidate(LoginFailureAttempts entity, Response<?> response, Mode mode) {
		// TODO Auto-generated method stub
		return true;
	}
}
