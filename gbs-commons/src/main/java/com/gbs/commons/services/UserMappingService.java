package com.gbs.commons.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gbs.commons.entity.UserMapping;
import com.gbs.commons.constants.Mode;
import com.gbs.commons.model.FilterCondition;
import com.gbs.commons.model.Response;
import com.gbs.commons.repository.UserMappingRepository;
import com.querydsl.core.types.Predicate;

@Service
public class UserMappingService extends AbstractService<UserMapping, Long> {

	@Autowired
	private UserMappingRepository userMappingRepository;

	public boolean deleteAll() {
		try {
			userMappingRepository.deleteAll();
			return true;
		} catch (Exception e) {
		}
		return false;
	}

	public UserMapping getDefaultRole(List<UserMapping> mappings) {

		if (mappings != null) {
			for (UserMapping userMapping : mappings) {
				if (userMapping.isDefaultRole()) {
					return userMapping;
				}
			}
		}
		return null;
	}

	public boolean isUserMappingListValid(List<UserMapping> mappingList) {

		int count = 0;
		if (mappingList != null) {
			for (UserMapping userMapping : mappingList) {

				if (userMapping.isDefaultRole())
					count++;
			}
		}
		return ((count == 1) ? true : false);
	}

	@Override
	protected Predicate createPredicate(List<FilterCondition> filterConditions) {
		// TODO Auto-generated method stub
		return null;
	}
	//new 

	@Override
	protected Boolean beValidate(UserMapping entity, Response<?> response, Mode mode) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Boolean bePopulate(UserMapping entity, Response<?> response, Mode mode) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Boolean businessValidate(UserMapping entity, Response<?> response, Mode mode) {
		// TODO Auto-generated method stub
		return null;
	}

}


