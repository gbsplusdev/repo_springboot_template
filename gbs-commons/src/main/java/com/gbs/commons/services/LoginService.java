/** ----------------------------------------------------------------------------------------------------------------
 * AUTHOR                          : Sandeep P Pillai
 * DATE                            : 07/02/2018
 * PURPOSE/NOTES                   : Login Service
 * IMPORTANT VARIABLES             :
 * ASSUMPTIONS/LIMITATIONS         :
 * GLOBAL VARIABLES USED           :
 * LIBRARY USED                    :  
 * CLASSES USED                    : 
 * ----------------------------------------------------------------------------------------------------------------
 * REVISION HISTORY
 * ----------------------------------------------------------------------------------------------------------------
 * Date                            By                                      Notes
 * ----------------------------------------------------------------------------------------------------------------
 * 
 * ----------------------------------------------------------------------------------------------------------------
 * PREFIX OF VARIABLES
 * GLOBAL                          : g_
 * CLASS                           : _
 * SUB/FUNCTION/PROPERTY/EVENT     : NIL
 * ARGUMENTS                       : a_ 
 * -----------------------------------------------------------------------------------------------------------------*/
package com.gbs.commons.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gbs.commons.constants.Mode;
import com.gbs.commons.entity.User;
import com.gbs.commons.model.FilterCondition;
import com.gbs.commons.model.Response;
import com.gbs.commons.repository.UserRepository;
import com.querydsl.core.types.Predicate;

@Service
public class LoginService extends AbstractService<User, Long> {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private UserMappingService userMappingService;

	public User findByUserName(String userName) {
		return userRepository.findByUserName(userName);
	}

	public boolean deleteAll() {
		try {
			userRepository.deleteAll();
			return true;
		} catch (Exception e) {
		}
		return false;
	}

	@Override
	protected Predicate createPredicate(List<FilterCondition> filterConditions) {
		return null;

//		QUser qUser = QUser.user;
//		BooleanBuilder booleanBuilder = new BooleanBuilder();
//		for (FilterCondition filterCondition : filterConditions) {
//
//			if ("email".equalsIgnoreCase(filterCondition.getAttribute())) {
//				booleanBuilder.and(qUser.email.equalsIgnoreCase(filterCondition.getValue()));
//			}
//		}
//		return booleanBuilder.getValue();

	}

	@Override
	protected Boolean beValidate(User entity, Response<?> response, Mode mode) {

		return (userMappingService.isUserMappingListValid(entity.getUserMappings()));
	}

	@Override
	protected Boolean bePopulate(User entity, Response<?> response, Mode mode) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	protected Boolean businessValidate(User entity, Response<?> response, Mode mode) {
		// TODO Auto-generated method stub
		return (userMappingService.isUserMappingListValid(entity.getUserMappings()));
	}

}
