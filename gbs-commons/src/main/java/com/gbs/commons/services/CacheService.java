/*--------------------------------------------------------------------------------------------------------------
AUTHOR                          : Aswathy Reghunath
DATE                            : 31/01/2018
PURPOSE/NOTES                   : Base service
IMPORTANT VARIABLES             :
ASSUMPTIONS/LIMITATIONS         :
GLOBAL VARIABLES USED           :
LIBRARY USED                    :  
CLASSES USED                    : 
----------------------------------------------------------------------------------------------------------------
REVISION HISTORY
----------------------------------------------------------------------------------------------------------------
Date                            By                                      Notes
----------------------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------------------
PREFIX OF VARIABLES
GLOBAL                          : g_
CLASS                           : _
SUB/FUNCTION/PROPERTY/EVENT     : NIL
ARGUMENTS                       : a_ 
--------------------------------------------------------------------------------------------------------------*/

package com.gbs.commons.services;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.gbs.commons.constants.ErrorType;
import com.gbs.commons.model.CacheSave;
import com.gbs.commons.model.Error;
import net.sf.ehcache.Element;

@Service
public class CacheService {
	@Autowired
	private CacheManager cacheManager;

	@Cacheable(key = "#toSaveInCache.key")
	public CacheSave lockEntityForEdit(CacheSave toSaveInCache, List<Error> listErrors) {
		Cache cache = cacheManager.getCache("editdatacache");
		Element element = (Element) cache.get(toSaveInCache.getKey());
		@SuppressWarnings("deprecation")
		Serializable value = element.getValue();
		if (value != null) {
			CacheSave savedInCache = new CacheSave();
			savedInCache = (CacheSave) value;
			Long savedUserId = savedInCache.getUserId();
			Long currentUserId = toSaveInCache.getUserId();
			if (savedUserId == currentUserId) {
				removeLock(savedInCache.getKey(), listErrors, savedUserId);
			} else {
				Date loginDate, currentDate;
				currentDate = new Date();
				loginDate = savedInCache.getLoginDate();
				Long timeDiff = currentDate.getTime() - loginDate.getTime();
				if (timeDiff > 1200000) {
					removeLock(savedInCache.getKey(), listErrors, savedUserId);
					Error errorData = new Error();
					errorData.setCode("sopt10001");// error code will be
													// modified when the
													// standard is defined
					errorData.setMessage("Cache Error");
					errorData.setTypes(ErrorType.ERROR);
					listErrors.add(errorData);

				}

			}
		}

		return toSaveInCache;

	}

	public final Boolean checkForLock(String keyName, List<Error> listErrors, Long userId) {
		boolean returnValue;
		Long cacheUserId, currentUserId;

		Cache cache = cacheManager.getCache("editdatacache");
		Element element = (Element) cache.get(keyName);
		@SuppressWarnings("deprecation")
		Serializable value = element.getValue();

		if (value == null) {
			Error errorData = new Error();
			errorData.setCode("sopt10001");// error code will be modified when
											// the standard is defined
			errorData.setMessage("Cache null Error");
			errorData.setTypes(ErrorType.ERROR);
			listErrors.add(errorData);

			returnValue = false;
		} else {
			CacheSave cacheSave = new CacheSave();
			cacheSave = (CacheSave) value;
			cacheUserId = cacheSave.getUserId();
			currentUserId = userId;
			if (currentUserId == cacheUserId) {
				returnValue = true;
			} else {
				Error errorData = new Error();
				errorData.setCode("sopt10001");// error code will be modified
												// when the standard is defined
				errorData.setMessage("Cache user error");
				errorData.setTypes(ErrorType.ERROR);
				listErrors.add(errorData);
				returnValue = false;
			}
		}
		return returnValue;

	}

	public final Boolean removeLock(String KeyName, List<Error> listErrors, Long userId) {
		boolean returnValue;
		Long cacheUserId, currentUserId;
		Cache cache = cacheManager.getCache("editdatacache");
		Element element = (Element) cache.get(KeyName);
		@SuppressWarnings("deprecation")
		Serializable value = element.getValue();
		if (value == null) {
			Error errorData = new Error();
			errorData.setCode("sopt10001");// error code will be modified when
											// the standard is defined
			errorData.setMessage("Cache null Error");
			errorData.setTypes(ErrorType.ERROR);
			listErrors.add(errorData);
			returnValue = false;
		} else {
			CacheSave cacheSave = new CacheSave();
			cacheSave = (CacheSave) value;
			cacheUserId = cacheSave.getUserId();
			currentUserId = userId;
			if (currentUserId == cacheUserId) {
				// need to be checked
				cache.evict(KeyName);
				returnValue = true;
			} else {
				Error errorData = new Error();
				errorData.setCode("sopt10001");// error code will be modified
												// when the standard is defined
				errorData.setMessage("Cache User Changed Error");
				errorData.setTypes(ErrorType.ERROR);
				listErrors.add(errorData);
				returnValue = false;
			}
		}
		return returnValue;
	}
}
