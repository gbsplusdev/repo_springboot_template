package com.gbs.commons.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gbs.commons.entity.ui.Label;
import com.gbs.commons.repository.LabelRepository;
import com.gbs.commons.util.LoginContext;
import com.gbs.commons.util.SaveResult;
import com.gbs.commons.util.ValidationResult;

@Service
public class LabelService implements Serviceable<Long, Label>{
	 
	@Autowired
	private LabelRepository labelRepository;

	@Override
	public List<ValidationResult> doValidate(Label obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Label findById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SaveResult save(Label label) {
		SaveResult result = new SaveResult();
		result.setSavedObject(labelRepository.save(label));
		return result;
	}

	@Override
	public SaveResult save(LoginContext context, Label obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SaveResult afterSave(Label obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SaveResult delete(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Label> getPaginatedList() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public Label findByLabelId(String labelId) {
		return labelRepository.findByLabelId(labelId);
	}


}
