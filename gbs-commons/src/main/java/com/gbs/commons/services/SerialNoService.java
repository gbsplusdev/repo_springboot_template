/*--------------------------------------------------------------------------------------------------------------
AUTHOR                          : Cimple Joseph
DATE                            : 08/02/2018
PURPOSE/NOTES                   : Service for Serial Number generation
IMPORTANT VARIABLES             :
ASSUMPTIONS/LIMITATIONS         :
GLOBAL VARIABLES USED           :
LIBRARY USED                    :  
CLASSES USED                    : 
----------------------------------------------------------------------------------------------------------------
REVISION HISTORY
----------------------------------------------------------------------------------------------------------------
Date                            By                                      Notes
----------------------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------------------
PREFIX OF VARIABLES
GLOBAL                          : g_
CLASS                           : _
SUB/FUNCTION/PROPERTY/EVENT     : NIL
ARGUMENTS                       : a_ 
--------------------------------------------------------------------------------------------------------------*/

package com.gbs.commons.services;

//@Service
public class SerialNoService {
		
//	@Autowired
//	SessionFactory sessionFactory;
//	
//	public void genSerialNo(String transType, String transSubType, int optionID, int companyID, int entityHierarchy,
//			int userID, int finYearID, boolean update, Integer noOfSerialNo, String serialNo) {
//
//		Query query=null;
//		int entityHierarchyID = entityHierarchy;
//		int rootRefSettingsID = 0;
//		String prefix = "";
//		String suffix = "";
//		Integer rootSettingsID = 0;
//		Integer refSettingsID = 0;
//		Integer sizeofGenNo;
//		String companyRequired, branchRequired, locationRequired, userRequired, finYearRequired, padChar;
//
//		if (noOfSerialNo == null) {
//			noOfSerialNo = 0;
//			// Raise error noOfSerialNo is not be zero
//		}
//
//		Session session = sessionFactory.openSession();
//		Transaction tx = session.beginTransaction();
//		query = session.createQuery(
//				"from MuzSerialNoSettings where optionID =:optionID and transSubType =:transSubType and transType =:transType");
//		query.setParameter("optionID", optionID);
//		query.setParameter("transSubType", transSubType);
//		query.setParameter("transType", transType);
//		/*query.setFirstResult(1);
//		query.setMaxResults(1);*/
//		MuzSerialNoSettings serialNoSettings = (MuzSerialNoSettings) query.uniqueResult();
//
//		if (transSubType == "SLNO") {
//
//			if (serialNoSettings == null) {
//				MuzSerialNoSettings serialNoSettings1 = new MuzSerialNoSettings();
//				serialNoSettings1.setOptionID(optionID);
//				serialNoSettings1.setTransType(transType);
//				serialNoSettings1.setTransSubType(transSubType);
//				serialNoSettings1.setBranchRequired("Y");
//				serialNoSettings1.setCompanyRequired("Y");
//				serialNoSettings1.setLocationRequired("Y");
//				serialNoSettings1.setUserRequired("N");
//				serialNoSettings1.setFinYearRequired("Y");
//				serialNoSettings1.setPrefix("SL00");
//				serialNoSettings1.setSuffix("");
//				serialNoSettings1.setPadChar("c");
//				serialNoSettings1.setSizeOfSerialNo(10);
//				serialNoSettings1.setRefSettingsID(null);
//				serialNoSettings1.setRemarks("");
//				session.saveOrUpdate(serialNoSettings1);
//
//				/* To fetch data from MuzSerialNoSettings */
//				query = null;
//				query = session.createQuery(
//						"from MuzSerialNoSettings where optionID =:optionID and transType =:transType and transSubType =:transSubType");
//				query.setParameter("optionID", optionID);
//				query.setParameter("transSubType", transSubType);
//				query.setParameter("transType", transType);
//				serialNoSettings=null;
//				serialNoSettings = (MuzSerialNoSettings) query.uniqueResult();
//			}
//
//		}
//		if (serialNoSettings != null) {
//			rootSettingsID = serialNoSettings.getSettingsID();
//			refSettingsID = serialNoSettings.getRefSettingsID();
//		} else {
//			refSettingsID = null;
//		}
//		while (refSettingsID != null) {
//			rootSettingsID = refSettingsID;
//			query = null;
//			serialNoSettings = null;
//			query = session.createQuery("from MuzSerialNoSettings where settingsID =: refSettingsID");
//			query.setParameter("refSettingsID", refSettingsID);
//			serialNoSettings = (MuzSerialNoSettings) query.uniqueResult();
//			refSettingsID = serialNoSettings.getRefSettingsID();
//		}
//
//		/* Select the row corresponding to RootSettingsID */
//		query = null;
//		serialNoSettings = null;
//		query = session
//				.createQuery("from MuzSerialNoSettings where settingsID =:rootRefSettingsID and optionID =:optionID");
//		query.setParameter("rootRefSettingsID", rootSettingsID);
//		query.setParameter("optionID", optionID);
//		serialNoSettings = (MuzSerialNoSettings) query.uniqueResult();
//		optionID = serialNoSettings.getOptionID();
//		transType = serialNoSettings.getTransType();
//		transSubType = serialNoSettings.getTransSubType();
//		companyRequired = serialNoSettings.getCompanyRequired();
//		branchRequired = serialNoSettings.getBranchRequired();
//		locationRequired = serialNoSettings.getLocationRequired();
//		userRequired = serialNoSettings.getUserRequired();
//		finYearRequired = serialNoSettings.getFinYearRequired();
//		prefix = serialNoSettings.getPrefix();
//		suffix = serialNoSettings.getSuffix();
//		padChar = serialNoSettings.getPadChar();
//		sizeofGenNo = serialNoSettings.getSizeOfSerialNo();
//
//		if (locationRequired == "N") {
//			entityHierarchyID = 1;// ParentEntityHierarchyID
//			if (branchRequired == "N") {
//				entityHierarchyID = 1;
//				if (companyRequired == "N") {
//					entityHierarchyID = 0;
//				}
//			}
//		}
//		if (userRequired == "N") {
//			userID = 0;
//		}
//		if (finYearRequired == "N") {
//			finYearID = 0;
//		}
//
//		if (transSubType == "SLNO") {
//			String prefix1 = prefix;
//			prefix = "";
//			query = null;
//			query = session.createQuery(
//					"from MuzSerialNo where settingsID=:rootSettingsID and userID =:userID and finYearID =:finyear and entityHierarchyID=:entityHierarchyID");
//			query.setParameter("rootSettingsID", rootSettingsID);
//			query.setParameter("userID", userID);
//			query.setParameter("finyear", finYearID);
//			query.setParameter("entityHierarchyID", entityHierarchyID);
//			MuzSerialNo serial = new MuzSerialNo();
//			serial = (MuzSerialNo) query.uniqueResult();
//			if (serial == null) {
//				prefix = "";
//			} else {
//				prefix = serial.getPrefix();
//			}
//			if (prefix == "") {
//				if (locationRequired == "false") {
//
//				}
//				if (userID != 0) {
//
//				}
//				if (finYearID != 0) {
//
//				}
//				prefix = prefix + prefix1;
//			}
//			if (prefix != "" & sizeofGenNo > 0) {
//				sizeofGenNo = sizeofGenNo + prefix1.length();
//			}
//		}
//		session.flush();
//		tx.commit();
//		session.close();
//		Map<Integer, String> serailno = generateMultipleSerialNoSub(rootSettingsID, entityHierarchyID, userID, finYearID, prefix,
//				suffix, padChar, sizeofGenNo, update, noOfSerialNo, serialNo);
//		System.out.println("Serial No : " + serailno);
//	}
//
//	private Map<Integer, String> generateMultipleSerialNoSub(Integer settingsId, Integer entityHierarchyID, Integer userID,
//			Integer finYearID, String prefix, String suffix, String padChar, Integer sizeofGenNo, Boolean update,
//			Integer noOfSerialNo, String serialNo) {
//
//		String genNo = null;
//		Integer rowNo;
//		Integer alternateKey = 0;
//		Query query = null;
//		serialNo = "0"; 
//		MuzSerialNo serial = new MuzSerialNo();
//		Map<Integer, String> tempSerailNo = new HashMap<Integer, String>();
//
//		Session session = sessionFactory.openSession();
//		Transaction tx = session.beginTransaction();
//		query = session.createQuery(
//				"from MuzSerialNo where settingsID =:settingsID and entityHierarchyID =:entityHierarchyID and userID =:userID and finYearID =:finYearID");
//		query.setParameter("settingsID", settingsId);
//		query.setParameter("entityHierarchyID", entityHierarchyID);
//		query.setParameter("userID", userID);
//		query.setParameter("finYearID", finYearID);
//		serial = (MuzSerialNo) query.uniqueResult();
//
//		if (serial != null) {
//			alternateKey = serial.getAlternateKey();
//		}
//
//		if (alternateKey == 0) {
//			MuzSerialNo serial1=new MuzSerialNo();
//			MuzSerialNoSettings serialNoSettings = new MuzSerialNoSettings();
//			serialNoSettings.setSettingsID(settingsId);
//			serial1.setSettingID(serialNoSettings);
//			serial1.setEntityHierarchyID(entityHierarchyID);
//			serial1.setUserID(userID);
//			serial1.setFinYearID(finYearID);
//			serial1.setLastSLNo(0);
//			serial1.setPrefix(prefix);
//			session.saveOrUpdate(serial1);
//
//			query = null;
//			query = session.createQuery(
//					"from MuzSerialNo where settingsID =:settingsID and entityHierarchyID =:entityHierarchyID and userID =:userID and finYearID =:finYearID");
//			query.setParameter("settingsID", settingsId);
//			query.setParameter("entityHierarchyID", entityHierarchyID);
//			query.setParameter("userID", userID);
//			query.setParameter("finYearID", finYearID);
//			// MuzSerialNo serial2 = new MuzSerialNo();
//			serial = (MuzSerialNo) query.uniqueResult();
//			alternateKey = serial.getAlternateKey();
//		}
//
//		query = null;
//		query = session.createQuery("from MuzSerialNo where alternateKey=:alternateKey");
//		query.setParameter("alternateKey", alternateKey);
//		serial = (MuzSerialNo) query.uniqueResult();
//		Integer lastNo = serial.getLastSLNo();
//		lastNo = lastNo + noOfSerialNo;
//		serialNo = String.valueOf(0 + noOfSerialNo);
//		if (update == true) {
//			query = null;
//			serial=null;
//			query = session.createQuery(
//					"update MuzSerialNo set lastSLNo =:lastNo, prefix =:prefix where alternateKey=:alternateKey");
//			query.setParameter("lastNo", lastNo);
//			query.setParameter("prefix", prefix);
//			query.setParameter("alternateKey", alternateKey);
//			query.executeUpdate();
//			serialNo = String.valueOf(lastNo + 1);
//		} else {
//			if (serial != null) {
//				serialNo = String.valueOf(serial.getLastSLNo() + noOfSerialNo);
//			}
//		}
//
//		if (noOfSerialNo == 1) {
//			genNo = prefix + serialNo + suffix;
//			int len = sizeofGenNo - genNo.length();
//			if (sizeofGenNo > genNo.length() & padChar != "") {
//				genNo = prefix + String.join("", Collections.nCopies(len, padChar)) + serialNo + suffix;
//			}
//
//			serialNo = genNo;
//			rowNo = 1;
//			tempSerailNo.put(rowNo, serialNo);
//		} else {
//			
//			rowNo = 1;
//			while (rowNo <= noOfSerialNo) {
//				genNo = prefix + serialNo + suffix;
//				int len = sizeofGenNo - genNo.length();
//				if (sizeofGenNo > genNo.length() & padChar != "") {
//					genNo = prefix + String.join("", Collections.nCopies(len, padChar)) + serialNo + suffix;
//				}
//				tempSerailNo.put(rowNo, genNo);
//				rowNo = rowNo + 1;
//				serialNo = String.valueOf(Integer.parseInt(serialNo) + 1);
//			}
//			System.out.println(tempSerailNo);
//		}
//		session.flush();
//		tx.commit();
//		session.close();
//		return tempSerailNo;
//
//	}
}
