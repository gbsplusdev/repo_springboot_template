package com.gbs.commons.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.querydsl.core.types.Predicate;

import com.gbs.commons.constants.Mode;
import com.gbs.commons.entity.Menu;

import com.gbs.commons.model.FilterCondition;
import com.gbs.commons.model.Response;
import com.gbs.commons.repository.MenuRepository;

@Service
public class MenuService extends AbstractService<Menu, Long>{
	public boolean deleteAll() {
		try {
			menuRepository.deleteAll();
			return true;	
		} catch (Exception e) {
		}
		return false;
	}
	@Autowired
	private MenuRepository menuRepository;

	@Override
	protected Predicate createPredicate(List<FilterCondition> filterConditions) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Boolean beValidate(Menu entity, Response<?> response, Mode mode) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	protected Boolean bePopulate(Menu entity, Response<?> response, Mode mode) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	protected Boolean businessValidate(Menu entity, Response<?> response, Mode mode) {
		// TODO Auto-generated method stub
		return true;
	}
		
	
}
