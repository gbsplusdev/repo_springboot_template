package com.gbs.commons.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gbs.commons.entity.UserAuthenticationToken;

public interface UserAuthenticationTokenRepository extends JpaRepository<UserAuthenticationToken, Long>{
	
	public UserAuthenticationToken findByAuthenticationToken(String authenticationToken);
	
}
