package com.gbs.commons.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.gbs.commons.entity.ui.Profile;

public interface ProfileRepository extends PagingAndSortingRepository<Profile,Long>{
	
	public Profile findByName(String name);

}
