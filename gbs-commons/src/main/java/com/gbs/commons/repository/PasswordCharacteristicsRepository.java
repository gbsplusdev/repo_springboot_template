package com.gbs.commons.repository;

import com.gbs.commons.entity.PasswordCharacteristics;

public interface PasswordCharacteristicsRepository extends AbstractRepository<PasswordCharacteristics, Long>{

}
