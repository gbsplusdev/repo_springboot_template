package com.gbs.commons.repository;


import org.springframework.stereotype.Repository;

import com.gbs.commons.entity.MenuConfig;

@Repository
public interface MenuConfigRepository extends AbstractRepository<MenuConfig, Long>{

		
}
