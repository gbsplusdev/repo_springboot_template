package com.gbs.commons.repository;


import org.springframework.stereotype.Repository;

import com.gbs.commons.entity.MenuGroup;

@Repository
public interface MenuGroupRepository extends AbstractRepository<MenuGroup, Long>  {
		
}
