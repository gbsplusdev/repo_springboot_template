/*--------------------------------------------------------------------------------------------------------------
AUTHOR                          : Yogesh H Shenoy
DATE                            : 31/01/2018
PURPOSE/NOTES                   : Base repository
IMPORTANT VARIABLES             :
ASSUMPTIONS/LIMITATIONS         :
GLOBAL VARIABLES USED           :
LIBRARY USED                    : querydsl 
CLASSES USED                    : PagingSortingRepository, QueryDslPredicateExecutor
----------------------------------------------------------------------------------------------------------------
REVISION HISTORY
----------------------------------------------------------------------------------------------------------------
Date                            By                                      Notes
----------------------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------------------
PREFIX OF VARIABLES
GLOBAL                          : g_
CLASS                           : _
SUB/FUNCTION/PROPERTY/EVENT     : NIL
ARGUMENTS                       : a_ 
--------------------------------------------------------------------------------------------------------------*/
package com.gbs.commons.repository;

import java.io.Serializable;

import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Base repository that implements PagingSortingRepository and
 * QueryDslPredicateExecutor
 * 
 * @param <E>
 *            entity type
 * @param <S>
 *            id type for the entity
 * 
 */

@NoRepositoryBean
public interface AbstractRepository<E, S extends Serializable>
		extends PagingAndSortingRepository<E, S>, QueryDslPredicateExecutor<E> {

}
