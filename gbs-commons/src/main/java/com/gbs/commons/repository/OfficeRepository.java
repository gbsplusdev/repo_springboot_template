package com.gbs.commons.repository;

import com.gbs.commons.entity.Office;

public interface OfficeRepository extends AbstractRepository<Office,Long>{

}
