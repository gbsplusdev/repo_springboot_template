package com.gbs.commons.repository;


import org.springframework.stereotype.Repository;

import com.gbs.commons.entity.MenuRoleMapping;


@Repository
public interface MenuRoleMappingRepository extends AbstractRepository<MenuRoleMapping,Long>{		
}
