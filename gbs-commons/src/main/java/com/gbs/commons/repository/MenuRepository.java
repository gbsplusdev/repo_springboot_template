package com.gbs.commons.repository;

import org.springframework.stereotype.Repository;

import com.gbs.commons.entity.Menu;

@Repository
public interface MenuRepository extends AbstractRepository<Menu, Long>{	
	
}
