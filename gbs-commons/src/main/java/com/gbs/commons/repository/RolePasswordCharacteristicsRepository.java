package com.gbs.commons.repository;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.gbs.commons.entity.RolePasswordCharacteristics;

public interface RolePasswordCharacteristicsRepository extends AbstractRepository<RolePasswordCharacteristics, Long> {		
	@Query("SELECT r FROM RolePasswordCharacteristics r WHERE r.role.id = :roleId")
    public RolePasswordCharacteristics findByRoleId(@Param("roleId") Long roleId);
}
