package com.gbs.commons.repository;

import com.gbs.commons.entity.UserLoginLog;

public interface UserLoginLogRepository extends AbstractRepository<UserLoginLog, Long> {

}
