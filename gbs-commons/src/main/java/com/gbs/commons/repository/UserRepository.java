package com.gbs.commons.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.gbs.commons.entity.User;

public interface UserRepository extends AbstractRepository<User, Long>, PagingAndSortingRepository<User, Long> {

	public User findByUserName(String userName);

	public User findByEmail(String email);
	
	public User findByAuthToken(String authToken);
	
	@Query("FROM User WHERE email=:email OR qualifiedNumber=:mobile")
	public User findByEmailOrMobile(@Param("email") String email, @Param("mobile") String mobile);

	@Query("FROM User WHERE email=:emailOrMobile OR qualifiedNumber=:emailOrMobile OR id=:emailOrMobile")
	public User findByEmailOrMobileOrUserId(@Param("emailOrMobile") String emailOrMobile);

}