package com.gbs.commons.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.gbs.commons.entity.LoginFailureAttempts;

public interface LoginFailureAttemptsRepository extends AbstractRepository<LoginFailureAttempts, Long> {

	@Query("SELECT l FROM LoginFailureAttempts l WHERE l.userId = :userId")
	public LoginFailureAttempts findByUserId(@Param("userId") Long userId);
}
