package com.gbs.commons.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.gbs.commons.entity.ui.Label;

@Repository
public interface LabelRepository extends PagingAndSortingRepository<Label,Long>{
	
	public Label findByLabelId(String labelId);
	
	

}
