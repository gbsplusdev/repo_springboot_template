package com.gbs.commons.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.gbs.commons.entity.Message;

@Repository
public interface MessageRepository extends PagingAndSortingRepository<Message, Long>{
	public Message findByMessageIdAndGroupId(String messageId,String groupId);
}
