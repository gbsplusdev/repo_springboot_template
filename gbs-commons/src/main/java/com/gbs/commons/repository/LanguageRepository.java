package com.gbs.commons.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.gbs.commons.entity.Language;

@Repository
public interface LanguageRepository extends PagingAndSortingRepository<Language, Long> {

	public Language findByDescription(String description);

}
