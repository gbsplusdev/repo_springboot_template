package com.gbs.commons.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.gbs.commons.entity.Role;
import com.gbs.commons.entity.User;

public interface RoleRepository extends AbstractRepository<Role, Long>, PagingAndSortingRepository<Role, Long> {
	@Query("FROM User WHERE name=:name AND description=:description")
	public User duplicateCheck(@Param("name") String name, @Param("description") String description);

	public Role findByname(String string);
}
